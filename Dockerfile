### STAGE 1: Build ###
FROM node:latest as builder
WORKDIR /app
COPY package*.json ./
RUN npm install --legacy-peer-deps
COPY . .
RUN npm run build
CMD ["npm", "run", "start"]
# # 2. For Nginx setup 
# FROM nginx:1.18.0-alpine
# COPY --from=builder /app/build /usr/share/nginx/html
# # Copy config nginx
# COPY --from=builder /app/nginx/nginx.conf /etc/nginx/conf.d/default.conf
# # Containers run nginx with global directives and daemon off
# CMD ["nginx", "-g", "daemon off;"]
