import AppMaterial from '@/material/appMaterial';
import {
  CalendarMonthRounded,
  EventAvailableRounded,
  LocationCityRounded,
  PeopleRounded,
  HomeWork,
  Output,
  Input
} from '@mui/icons-material';
import {
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Skeleton,

} from '@mui/material';
import { useTranslations } from 'next-intl';
import { usePathname, useRouter } from 'next/navigation';
import { ReactNode } from 'react';

interface MenuListType {
  label: string;
  icon: ReactNode;
  path: string;
}

const menuList: MenuListType[] = [
  {
    label: 'users',
    icon: <PeopleRounded />,
    path: '/main/users',
  },
  {
    label: 'schedules',
    icon: <CalendarMonthRounded />,
    path: '/main/schedules',
  },
  {
    label: 'departments',
    icon: <LocationCityRounded />,
    path: '/main/departments',
  },
  {
    label: 'classes',
    icon: <HomeWork />,
    path: '/main/classes',
  },
  {
    label: 'checkIn',
    icon: <Input />,
    path: '/main/checkin',
  },
  {
    label: 'checkOut',
    icon: <Output />,
    path: '/main/checkout',
  },
];

export default function MenuListWidget({
  currentUser,
}: {
  currentUser: UserModel;
}) {
  const t = useTranslations();
  const pathname = usePathname();
  const router = useRouter();

  return (
    <List sx={{ pl: AppMaterial.spacing.verySmall }}>
      {menuList.map((menuItem, index) => {
        const isSelected = menuItem.path == pathname;
        const menuCondition = !currentUser.isAdmin
          ? index != 0 && index != 2
          : true;

        return (
          menuCondition && (
            <ListItem
              key={index}
              disablePadding
              sx={{
                '& .MuiListItemButton-root': {
                  ':hover': {
                    backgroundColor: !isSelected
                      ? 'rgba(0, 0, 0, 0.1)'
                      : AppMaterial.colors.primary,
                  },
                },
              }}
            >
              {currentUser.id ? (
                <ListItemButton
                  sx={{
                    borderRadius: '8px 0 0 8px',
                    backgroundColor: isSelected
                      ? AppMaterial.colors.primary
                      : 'transparent',
                    cursor: isSelected ? 'default' : 'pointer',
                  }}
                  onClick={() => !isSelected && router.replace(menuItem.path)}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 32,
                      '& svg': {
                        width: 24,
                        color: '#FFF',
                      },
                    }}
                  >
                    {menuItem.icon}
                  </ListItemIcon>
                  <ListItemText primary={t(menuItem.label)} />
                </ListItemButton>
              ) : (
                <ListItemButton sx={{ minWidth: '100%' }}>
                  <ListItemIcon sx={{ minWidth: 32 }}>
                    <Skeleton variant='circular' width={24} height={24} />
                  </ListItemIcon>
                  <ListItemText
                    primary={<Skeleton variant='rounded' height={16} />}
                  />
                </ListItemButton>
              )}
            </ListItem>
          )
        );
      })}
    </List>
  );
}
