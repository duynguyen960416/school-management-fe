import AppMaterial from '@/material/appMaterial';
import AppUtils from '@/utils/appUtils';
import { Avatar, Skeleton, Stack, Typography } from '@mui/material';

export default function UserInfomationWidget({
  currentUser,
}: {
  currentUser: UserModel;
}) {
  return (
    <Stack direction='row' sx={{ p: AppMaterial.spacing.verySmall }}>
      {currentUser.id ? (
        <Avatar
          sx={{
            bgcolor: AppUtils.stringToColor(currentUser.fullName ?? ''),
            fontSize: 18,
            fontWeight: 'bold',
          }}
        >
          {AppUtils.stringAvatar(currentUser.fullName ?? '')}
        </Avatar>
      ) : (
        <Skeleton variant='circular' width={40} height={40} />
      )}
      <Stack
        sx={{
          flexGrow: 1,
          ml: AppMaterial.spacing.verySmall,
          justifyContent: 'center',
        }}
      >
        <Typography
          sx={{
            fontSize: AppMaterial.fontSize.small,
            fontWeight: 'bold',
          }}
        >
          {currentUser.id ? currentUser.fullName : <Skeleton />}
        </Typography>
        <Typography
          sx={{
            fontSize: AppMaterial.fontSize.verySmall,
            fontWeight: '600',
            color: AppMaterial.colors.gray,
          }}
        >
          {currentUser.id ? (
            currentUser.studentCode
          ) : (
            <Skeleton width={'50%'} />
          )}
        </Typography>
      </Stack>
    </Stack>
  );
}
