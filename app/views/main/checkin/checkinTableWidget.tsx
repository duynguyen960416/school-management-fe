'use client';

import { Box } from '@mui/material';
import useCheckinTableWidget from './_useCheckinTableWidget';
import AppTable from '@/components/appTable';
import AppMaterial from '@/material/appMaterial';

export default function CheckinTableWidget({
  onCellClick,
  onItemSelection,
  searchText,
}: {
  onCellClick: (e: any) => void;
  onItemSelection: (e: any) => void;
  searchText: string;
}) {
  const { t, columns, gridLocalization,checkin,listUser } =
  useCheckinTableWidget();

  return (
    <Box
      sx={{ height: 'calc(100vh - 88px)', mt: AppMaterial.spacing.verySmall }}
      display='grid'
    >
      <AppTable
        rows={checkin.filter(
          (el) => el.user &&
           listUser.filter((user)=>  user.id && user.fullName === searchText )
         )}
        //  rows={checkin}
        columns={columns}
        gridLocalization={gridLocalization}
        onCellClick={onCellClick}
        onItemSelection={onItemSelection}
      />
    </Box>
  );
}
