import ApiDictionaries from "@/_api/api-dictionaries";
import axiosClient from "@/_api/axios-client";
import AppButton from "@/components/appButton";
import {
  currentUser,
  departmentList,
  rfidCardList,
  scheduleList,
} from "@/store/appStore";
import AppUtils from "@/utils/appUtils";
import { GridColDef, enUS, viVN } from "@mui/x-data-grid";
import { Localization } from "@mui/x-data-grid/utils/getGridLocalization";
import dayjs from "dayjs";
import { useAtom } from "jotai";
import { useLocale, useTranslations } from "next-intl";
import { useEffect } from "react";
import { useSignal, useSignalAndValue } from "signals-react-safe";

type CheckInType = {
  id: number;
  option: "check-in" | "check-out";
};

export default function useDepartmentsTableWidget() {
  const t = useTranslations();
  const locale = useLocale();
  const gridLocalization = useSignal<Localization>(viVN);
  const [user] = useAtom(currentUser);
  const [departments] = useAtom(departmentList);
  const [cardIds] = useAtom(rfidCardList);
  const [schedules] = useAtom(scheduleList);
  const [ws, wsValue] = useSignalAndValue<WebSocket | null>(null);
  const [cardId, cardIdValue] = useSignalAndValue<string>("");

  const [cardOption, cardOptionValue] = useSignalAndValue<CheckInType | null>(
    null
  );
  const [checkInCount, checkInCountValue] = useSignalAndValue<number>(0);

  useEffect(() => {
    gridLocalization.value = locale == "vi" ? viVN : enUS;

    // WS config
    const socket = new WebSocket("ws://172.20.10.10:8088");

    socket.onopen = () => {
      socket.send("stop_check_in");
    };

    socket.onmessage = (event) => {
      if (event.data.includes("card_data")) {
        const newCardId = event.data.replace("card_data: ", "").trim();
        cardId.value = newCardId;
        checkInCount.value += 1;

        onCheckIn(newCardId);
      }
    };

    ws.value = socket;

    return () => {
      socket.close();
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [gridLocalization, locale]);

  const columns: GridColDef[] = [
    { field: "name", headerName: t("name"), flex: 1 },
    {
      field: "isClass",
      headerName: t("class"),
      flex: 1,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        return params.value ? t("yes") : t("no");
      },
    },
    {
      field: "capacity",
      headerName: t("capacity"),
      flex: 1,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "checkIn",
      headerName: t("checkIn"),
      flex: 1,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        const isCheckIn =
          cardOptionValue?.id === params.row.id &&
          cardOptionValue?.option === "check-in";
        return (
          <AppButton
            sx={{ height: 24 }}
            onClick={(e) => onWaitingCardData(e, "check-in", params.row.id)}
            disabled={cardOptionValue != null && !isCheckIn}
          >
            {isCheckIn ? `${t("stop")} (${checkInCountValue})` : t("checkIn")}
          </AppButton>
        );
      },
    },
    {
      field: "checkOut",
      headerName: t("checkOut"),
      flex: 1,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        const isCheckOut =
          cardOptionValue?.id === params.row.id &&
          cardOptionValue?.option === "check-out";

        return (
          <AppButton
            color="warning"
            sx={{ height: 24 }}
            onClick={(e) => onWaitingCardData(e, "check-out", params.row.id)}
            disabled={cardOptionValue != null && !isCheckOut}
          >
            {isCheckOut ? `${t("stop")} (${checkInCountValue})` : t("checkOut")}
          </AppButton>
        );
      },
    },
  ];

  const onWaitingCardData = (
    e: any,
    option: "check-in" | "check-out",
    deparmentId: number
  ) => {
    e.stopPropagation();
    if (wsValue) {
      if (cardOptionValue == null) {
        cardOption.value = { id: deparmentId, option: option };
        wsValue?.send("start_check_in");
      } else {
        cardOption.value = null;
        checkInCount.value = 0;
        wsValue?.send("stop_check_in");
      }
    }
  };

  const onCheckIn = async (cardId: string) => {
    const cardData = cardIds.find((item) => item.cardId === cardId);
    const dateTime = dayjs().format("YYYY-MM-DD HH:mm:ss");
    const date = dayjs(dateTime).format("DD-MM-YYYY");
    const time = dayjs(dateTime).format("HH:mm:ss");
    const schedule = schedules.find((item) => {
      if (
        item.date === date &&
        AppUtils.isTimeInRange(time, item.time, item.duration)
      ) {
        return item;
      }
    });

    if (cardData) {
      if (cardOption.value?.option === "check-in") {
        // let dataRequest: any;
        // console.log("schedule: ", schedule);
        if (schedule) {
          console.log("có schedule");
          const dataRequest = await axiosClient.post(ApiDictionaries.CHECK_IN(), {
            data: {
              dateTime: dayjs().format("YYYY-MM-DD HH:mm:ss"),
              user: cardData.user,
              department: cardOption.value?.id,
              schedule: schedule,
            },
          });
          console.log("data: ", dataRequest.data.attributes.user.data.id);
          const validShedule = schedules.find((item:any) => item.id === schedule.id)
          
          const newScheduleUser = validShedule?.users?.includes(dataRequest.data.attributes.user.id) ? validShedule?.users : validShedule?.users.push(dataRequest.data.attributes.user.data.id);
          console.log("validShedule: ", validShedule?.users);
          
          
          
          if (dataRequest) {
            const updateSchedule = await axiosClient.put(
              ApiDictionaries.SCHEDULE(schedule.id?.toString()),
              {
                data: {
                  ...schedule,
                  date: dayjs(dateTime).format("YYYY-MM-DD"),
                  users: validShedule?.users
                },
              }
            );
            if (updateSchedule) {
              console.log("OK", updateSchedule);
            }
          }
        }
        /*  else {
          console.log("đéo có schedule");
          data = await axiosClient.post(ApiDictionaries.CHECK_IN(), {
            data: {
              dateTime: dateTime,
              user: cardData.user,
              department: cardOption.value?.id,
              schedule: null,
            },
          });
        }
        if (data) {
          console.log(data);
        } */
      } else if (cardOption.value?.option === "check-out") {
        const data = await axiosClient.post(ApiDictionaries.CHECK_OUT(), {
          data: {
            dateTime: dayjs().format("YYYY-MM-DD HH:mm:ss"),
            user: cardData.user,
            department: cardOption.value?.id,
            schedule: schedule ? schedule : null,
          },
        });
      }
    }
  };

  return {
    t,
    columns,
    gridLocalization,
    departments,
  };
}
