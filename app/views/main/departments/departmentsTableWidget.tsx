'use client';

import { Box } from '@mui/material';
import AppTable from '@/components/appTable';
import AppMaterial from '@/material/appMaterial';
import useDepartmentsTableWidget from './_useDepartmentsTableWidget';

export default function DepartmentsTableWidget({
  onCellClick,
  onItemSelection,
  searchText,
}: {
  onCellClick: (e: any) => void;
  onItemSelection: (e: any) => void;
  searchText: string;
}) {
  const { t, columns, gridLocalization, departments } =
    useDepartmentsTableWidget();
  return (
    <Box
      sx={{ height: 'calc(100vh - 88px)', mt: AppMaterial.spacing.verySmall }}
      display='grid'
    >
      <AppTable
        rows={departments.filter(
          (el) =>
            el.name.includes(searchText)
        
        )}
        columns={columns}
        gridLocalization={gridLocalization}
        onCellClick={onCellClick}
        onItemSelection={onItemSelection}
      />
    </Box>
  );
}
