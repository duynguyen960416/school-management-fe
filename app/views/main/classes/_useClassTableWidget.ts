import { classesList, departmentList } from '@/store/appStore';
import { GridColDef, enUS, viVN } from '@mui/x-data-grid';
import { Localization } from '@mui/x-data-grid/utils/getGridLocalization';
import { useAtom } from 'jotai';
import { useLocale, useTranslations } from 'next-intl';
import { useEffect } from 'react';
import { useSignal } from 'signals-react-safe';

export default function useClassesTableWidget() {
  const t = useTranslations();
  const locale = useLocale();
  const gridLocalization = useSignal<Localization>(viVN);
  const [classes] = useAtom(classesList);

  useEffect(() => {
    gridLocalization.value = locale == 'vi' ? viVN : enUS;
  }, [gridLocalization, locale]);

  const columns: GridColDef[] = [
    { field: 'name', headerName: t('name'), flex: 1 },
    {
      field: 'startDate',
      headerName: 'Start Date',
      flex: 1,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'endDate',
      headerName: 'End Date',
      flex: 1,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'users',
      headerName: 'Users',
      flex: 1,
      align: 'center',
      headerAlign: 'center',
        renderCell: (params) => {
            return params.row.users.length;
        },
    },
  ];

  return {
    t,
    columns,
    gridLocalization,
    classes,
  };
}
