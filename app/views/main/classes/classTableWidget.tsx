'use client';

import { Box } from '@mui/material';
import AppTable from '@/components/appTable';
import AppMaterial from '@/material/appMaterial';
import useClassesTableWidget from './_useClassTableWidget';

export default function DepartmentsTableWidget({
  onCellClick,
  onItemSelection,
  searchText,
}: {
  onCellClick: (e: any) => void;
  onItemSelection: (e: any) => void;
  searchText: string;
}) {
  const { t, columns, gridLocalization, classes } =
    useClassesTableWidget();
  return (
    <Box
      sx={{ height: 'calc(100vh - 88px)', mt: AppMaterial.spacing.verySmall }}
      display='grid'
    >
      <AppTable
        rows={classes.filter(
          (el) =>
            el.name.includes(searchText)
        
        )}
        columns={columns}
        gridLocalization={gridLocalization}
        onCellClick={onCellClick}
        onItemSelection={onItemSelection}
      />
    </Box>
  );
}
