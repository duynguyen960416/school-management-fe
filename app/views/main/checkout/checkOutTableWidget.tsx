'use client';

import { Box } from '@mui/material';
import AppTable from '@/components/appTable';
import AppMaterial from '@/material/appMaterial';
import useCheckOutTableWidget from './_useCheckOutTableWidget';

export default function CheckOutTableWidget({
  onCellClick,
  onItemSelection,
  searchText,
}: {
  onCellClick: (e: any) => void;
  onItemSelection: (e: any) => void;
  searchText: string;
}) {
  const { t, columns, gridLocalization,checkout,listUser } =
  useCheckOutTableWidget();

  return (
    <Box
      sx={{ height: 'calc(100vh - 88px)', mt: AppMaterial.spacing.verySmall }}
      display='grid'
    >
      <AppTable
         rows={checkout}
        columns={columns}
        gridLocalization={gridLocalization}
        onCellClick={onCellClick}
        onItemSelection={onItemSelection}
      />
    </Box>
  );
}
