import { checkInList, checkOutList, currentUser, departmentList, scheduleList, userList } from '@/store/appStore';
import { GridColDef, enUS, viVN } from '@mui/x-data-grid';
import { Localization } from '@mui/x-data-grid/utils/getGridLocalization';
import dayjs from 'dayjs';
import { useAtom } from 'jotai';
import { useLocale, useTranslations } from 'next-intl';
import { useEffect, useRef, useState } from 'react';
import { useSignal, useSignalAndValue } from 'signals-react-safe';

export default function useCheckOutTableWidget() {
  const t = useTranslations();
  const locale = useLocale();
  const gridLocalization = useSignal<Localization>(viVN);
  const [checkout] = useAtom(checkOutList);
  const [current_user] = useAtom(currentUser);
  const [listUser] = useAtom(userList);
  const [schedule] = useAtom(scheduleList);
  const [department] = useAtom(departmentList);

  const [colDepartmentData, setColDepartmentData] = useState<any[]>([
  ]);


  useEffect(() => {
    const allCheckIns = listUser.flatMap(user => user.checkIn);
    const allCheckinIds = allCheckIns.map(checkIn => checkIn.id);

    const departments = allCheckinIds.map(checkInId => {
    const foundScheduleItem = schedule.find(scheduleItem =>
      scheduleItem.checkIn.includes(checkInId as any)
    );
    if (foundScheduleItem) {
      const departmentId = foundScheduleItem.department;
      const matchingDepartment = department.find(dept => dept.id === departmentId);
      if (matchingDepartment) {

        return matchingDepartment;
      }
    }
  });

  setColDepartmentData(departments);
 
  gridLocalization.value = locale == 'vi' ? viVN : enUS;
  },
    [gridLocalization, locale, schedule, checkout, listUser, department]);

  const columns: GridColDef[] = [
    { field: 'id', headerName: t('name'), flex: 1 },
    {
      field: 'user',
      headerName: 'User',
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      renderCell: (params) => {
        const userData = listUser.find(user => user.id === params.row.user);
        return userData ? userData.fullName : "Không xác định";
      },
    },
    {
      field: 'department',
      headerName: 'Department',
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      renderCell: (params) => {
        
        const departmentData = colDepartmentData.find(dept => dept && dept.id === params.row.department);
        return departmentData ? departmentData.name : "Không xác định";
      },
    },
    
    {
      field: 'dateTime',
      headerName: t('date'),
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      renderCell: (params) => {
        return params.row.dateTime
      }
    }
  ];

  return {
    t,
    columns,
    gridLocalization,
    checkout,
    listUser
  };
}
