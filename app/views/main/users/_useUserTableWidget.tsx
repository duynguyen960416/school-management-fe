import { userList } from '@/store/appStore';
import {
  GridColDef,
  enUS,
  viVN
} from '@mui/x-data-grid';
import { Localization } from '@mui/x-data-grid/utils/getGridLocalization';
import { useAtom } from 'jotai';
import { useLocale, useTranslations } from 'next-intl';
import { useEffect } from 'react';
import { useSignal } from 'signals-react-safe';

export default function useUserTableWidget() {
  const t = useTranslations();
  const locale = useLocale();
  const gridLocalization = useSignal<Localization>(viVN);
  const [usersList] = useAtom(userList);

  useEffect(() => {
    gridLocalization.value = locale == 'vi' ? viVN : enUS;
  }, [gridLocalization, locale]);

  const columns: GridColDef[] = [
    { field: 'fullName', headerName: t('fullName'), flex: 1 },
    {
      field: 'studentCode',
      headerName: t('studentCode'),
      flex: 1,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'dob',
      headerName: t('dob'),
      flex: 1,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'email',
      headerName: t('email'),
      flex: 1,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'phoneNumber',
      headerName: t('phoneNumber'),
      flex: 1,
      align: 'right',
      headerAlign: 'right',
    },
  ];

  return {
    t,
    columns,
    gridLocalization,
    usersList,
  };
}
