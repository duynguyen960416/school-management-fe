import AppTable from '@/components/appTable';
import AppMaterial from '@/material/appMaterial';
import { Box } from '@mui/material';
import useUserTableWidget from './_useUserTableWidget';

export default function UserTableWidget({
  onCellClick,
  onItemSelection,
  searchText,
}: {
  onCellClick: (e: any) => void;
  onItemSelection: (e: any) => void;
  searchText: string;
}) {
  const { t, columns, gridLocalization, usersList } = useUserTableWidget();

  return (
    <Box
      sx={{ height: 'calc(100vh - 88px)', mt: AppMaterial.spacing.verySmall }}
      display='grid'
    >
      <AppTable
        rows={usersList.filter(
          (el) =>
            el.fullName.includes(searchText) ||
            el.studentCode.includes(searchText) ||
            el.email.includes(searchText) ||
            el.phoneNumber.includes(searchText)
        )}
        columns={columns}
        gridLocalization={gridLocalization}
        onCellClick={onCellClick}
        onItemSelection={onItemSelection}
      />
    </Box>
  );
}
