import { scheduleList } from '@/store/appStore';
import { useAtom } from 'jotai';
import { useTranslations } from 'next-intl';

export default function useScheduleTableWidget() {
  const t = useTranslations();

  return { t };
}
