'use client';

import AppMaterial from '@/material/appMaterial';
import { departmentList, subjectList } from '@/store/appStore';
import { Box, Divider, Grid, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import dayjs from 'dayjs';
import { useAtom } from 'jotai';
import moment from 'moment';
import useScheduleTableWidget from './_useScheduleTableWidget';
import AppButton from '@/components/appButton';

const dayOfWeeks: string[] = ['t2', 't3', 't4', 't5', 't6', 'activities'];
const titleStyle = { color: AppMaterial.colors.warning };

export default function ScheduleTableWidget({
  data,
  onClick,
}: {
  data: ScheduleModel[];
  onClick: (schedule: ScheduleModel) => void;
}) {
  const { t } = useScheduleTableWidget();
  const [subjects] = useAtom(subjectList);
  const [departments] = useAtom(departmentList);

  return (
    <Box
      sx={{
        height: 'calc(100vh - 88px)',
        mt: AppMaterial.spacing.verySmall,
        overflow: 'auto',
      }}
    >
      <Grid container spacing={3} alignItems='stretch'>
        {dayOfWeeks.map((item, index) => {
          const schedules = data.filter(
            (scheduleItem) =>
              moment(scheduleItem.date, 'DD-MM-YYYY').day() == index + 1
          );
          schedules.sort((a, b) =>
            moment(a.time, 'HH:mm').diff(moment(b.time, 'HH:mm'))
          );

          return (
            <Grid key={index} item xs={2}>
              <Stack
                sx={{
                  backgroundColor: AppMaterial.colors.background1,
                  borderRadius: 2,
                  px: AppMaterial.spacing.verySmall / 3,
                  py: AppMaterial.spacing.verySmall / 2,
                  minHeight: 350,
                  height: '100%',
                }}
              >
                <Typography sx={{ textAlign: 'center', fontWeight: 600 }}>
                  {t(item)}
                </Typography>
                <Divider sx={{ pt: 2 }} />
                {schedules.length > 0 &&
                  schedules.map((scheduleItem, scheduleIndex) => {
                    const subject = subjects.find(
                      (el) => el.id == scheduleItem?.subject
                    );
                    const department = departments.find(
                      (el) => el.id == scheduleItem?.department
                    );

                    return (
                      <AppButton
                        key={scheduleIndex}
                        color='primary'
                        sx={{
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'flex-start',
                          alignItems: 'flex-start',
                          textAlign: 'left',
                          mt: AppMaterial.spacing.verySmall / 2,
                          p: AppMaterial.spacing.verySmall / 2,
                        }}
                        onClick={() => onClick(scheduleItem)}
                      >
                        <Typography
                          sx={{ fontSize: AppMaterial.fontSize.small }}
                        >
                          <span style={titleStyle}>{t('subject')}: </span>
                          {subject?.name}
                        </Typography>
                        <Typography
                          sx={{ fontSize: AppMaterial.fontSize.small }}
                        >
                          <span style={titleStyle}>{t('room')}: </span>
                          {department?.name}
                        </Typography>
                        <Typography
                          sx={{ fontSize: AppMaterial.fontSize.small }}
                        >
                          <span style={titleStyle}>{t('credit')}: </span>
                          {subject?.credit}
                        </Typography>
                        <Typography
                          sx={{ fontSize: AppMaterial.fontSize.small }}
                        >
                          <span style={titleStyle}>{t('duration')}: </span>
                          {`${scheduleItem?.duration} ${t('min')}`}
                        </Typography>
                        <Typography
                          sx={{ fontSize: AppMaterial.fontSize.small }}
                        >
                          <span style={titleStyle}>{t('startTime')}: </span>
                          {dayjs(scheduleItem?.time, 'HH:mm').format('HH:mm')}
                        </Typography>
                      </AppButton>
                    );
                  })}
              </Stack>
            </Grid>
          );
        })}
      </Grid>
    </Box>
  );
}
