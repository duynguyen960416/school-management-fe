export default class ApiDictionaries {
  public static readonly LOGIN = (value?: string) => `auth/local${value ? `/${value}` : ''}?populate=*`;
  public static readonly PROFILE = (value?: string) => `users/me${value ? `/${value}` : ''}?populate=*`;
  public static readonly USER = (value?: string) => `users${value ? `/${value}` : ''}?populate=*`;
  public static readonly DEPARTMENT = (value?: string) => `departments${value ? `/${value}` : ''}?populate=*`;
  public static readonly CLASSES = (value?: string) => `classes${value ? `/${value}` : ''}?populate=*`;
  public static readonly RFID_CARD = (value?: string) => `rfid-cards${value ? `/${value}` : ''}?populate=*`;
  public static readonly SCHEDULE = (value?: string) => `schedules${value ? `/${value}` : ''}?populate=*`;
  public static readonly SUBJECT = (value?: string) => `subjects${value ? `/${value}` : ''}?populate=*`;
  public static readonly WEEK = (value?: string) => `weeks${value ? `/${value}` : ''}?populate=*`;
  public static readonly CHECK_IN = (value?: string) => `check-ins${value ? `/${value}` : ''}?populate=*`;
  public static readonly CHECK_OUT = (value?: string) => `check-outs${value ? `/${value}` : ''}?populate=*`;
}