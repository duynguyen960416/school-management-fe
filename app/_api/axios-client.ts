import axios from 'axios';
import queryString from 'query-string';

const axiosClient = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_ENDPOINT}/api`,
  headers: {
    'content-type': 'application/json',
  },
  paramsSerializer: (params) => queryString.stringify(params),
});

axiosClient.interceptors.request.use(async (config) => {
  // Handle token here ...
  // const token =
  // const token = getToken();
  // const decodedToken = jwtDecode(token) as JwtPayload;
  // console.log(decodedToken);
  // console.log(moment.utc().valueOf());
  // console.log(new Date().getTime());
  // if (decodedToken * 1000 < moment.utc().valueOf()) {
  //   console.log("Token expired.");
  // } else {
  //   console.log("Valid token");
  //   result = true;
  // }
  // console.log("interceptors.request");
  return config;
});

axiosClient.interceptors.response.use(
  (response) => {
    // console.log("interceptors.response");
    if (response && response.data) {
      if (response.headers['content-type'] === 'application/zip') {
        return response;
      }
      return response.data;
    }

    return response;
  },
  (error) => {
    // Handle errors
    console.error(error);
    return null;
  }
);

export default axiosClient;
