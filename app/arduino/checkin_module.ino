#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>

const char *ssid = "Khanh Linh"; //Thay bằng ssid của chúng ta
const char *password = "20051991"; //thay bằng mật khẩu của wifi

bool isCheckIn = false;
bool requestCardId = false;
unsigned long lastScanTime = 0;
uint8_t requestingClientNum = 0;

IPAddress staticIP(192, 168, 1, 204);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

WebSocketsServer webSocket = WebSocketsServer(8088);

void webSocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t length) {
  switch (type) {
    case WStype_DISCONNECTED:
      Serial.printf("[%u] Disconnected!\n", num);
      break;
    case WStype_CONNECTED:
      {
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
      }
      break;
    case WStype_TEXT:
      String receivedMessage = String((char *)payload);
      Serial.printf("[%u] get Text: %s\n", num, receivedMessage.c_str());

      // CHECKIN - CHECKOUT
      if (receivedMessage.equals("start_check_in")) {
        Serial.printf("[%u] get Text get_card_id: %s\n", num, receivedMessage.c_str());
        requestingClientNum = num;
        isCheckIn = true;
      }

      if (receivedMessage.equals("stop_check_in")) {
        isCheckIn = false;
        requestingClientNum = 0;
      }

      // REGISTER CARD
      if (receivedMessage.equals("get_card_id")) {
        Serial.printf("[%u] get Text get_card_id: %s\n", num, receivedMessage.c_str());
        requestingClientNum = num;
        requestCardId = true;
      }
      // webSocket.sendTXT(num, payload);
      break;
  }
}

void setup() {
  Serial.begin(115200);
  while (!Serial);

  WiFi.config(staticIP, gateway, subnet);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Wifi connecting...");
  }

  Serial.println("WiFi Connected!!");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}

void loop() {
  unsigned long currentTime = millis();
  if (Serial.available() > 0) {
    String receivedData = Serial.readStringUntil('\n');

    // CHECKIN - CHECKOUT
    if (isCheckIn && !receivedData.isEmpty() && receivedData.indexOf("card_data") != -1) {
      webSocket.sendTXT(requestingClientNum, receivedData);

      lastScanTime = currentTime;
      delay(500);
    }

    // REGISTER CARD
    if (requestCardId && !receivedData.isEmpty() && receivedData.indexOf("card_data") != -1) {
      webSocket.sendTXT(requestingClientNum, receivedData);
      requestingClientNum = 0;

      lastScanTime = currentTime;
      requestCardId = false;
      delay(500);
    }
  }
  webSocket.loop();
}


// Code for Arduino

// #include <SPI.h>
// #include <MFRC522.h>

// #define RST_PIN         9
// #define SS_PIN          10

// MFRC522 mfrc522(SS_PIN, RST_PIN);
// byte readCard[4];

// void setup() {
//   Serial.begin(115200);
//   while (!Serial);

//   SPI.begin();
//   mfrc522.PCD_Init();
//   delay(4);
//   mfrc522.PCD_DumpVersionToSerial();
//   Serial.println(F("Ready"));
// }

// void loop() {
//   if ( ! mfrc522.PICC_IsNewCardPresent()) {
//     return;
//   }
//   if ( ! mfrc522.PICC_ReadCardSerial()) {
//     return;
//   }

//   String cardData = "";
//   for (uint8_t i = 0; i < 4; i++) {
//     readCard[i] = mfrc522.uid.uidByte[i];
//     cardData += String(readCard[i], HEX);
//   }
//   // Serial.println("Card Data: " + cardData);
//   Serial.print("card_data: " + cardData);
//   Serial.println();

//   mfrc522.PICC_HaltA();
// }
