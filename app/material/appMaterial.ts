export default class AppMaterial {
  static colors = {
    primary: '#151C2D',
    secondary: '#047A79',
    background1: '#1D263D',
    warning: '#FF9966',
    error: '#FF3333',
    success: '#22BB33',
    info: '#5BC0DE',
    gray: '#A4A4A4',
  };

  static spacing = {
    verySmall: 4,
    small: 6,
    normal: 8,
    medium: 12,
    large: 16,
  };

  static fontSize = {
    verySmall: 10,
    small: 12,
    normal: 14,
    medium: 18,
    large: 24,
  };

  static inputStyle = {
    '& .MuiFormLabel-root': {
      color: 'rgba(255, 255, 255, 0.4)',
      '&.Mui-focused': {
        color: 'rgba(255, 255, 255, 0.8)',
      },
      '&.Mui-disabled': {
        color: 'rgba(255, 255, 255, 0.4)',
      },
    },

    '& .MuiInputBase-root': {
      backgroundColor: '#121826',

      '& .MuiOutlinedInput-notchedOutline': {
        borderColor: 'rgba(255, 255, 255, 0.4)',
        borderWidth: 1,
      },
      ':hover': {
        '& .MuiOutlinedInput-notchedOutline': {
          borderColor: 'rgba(255, 255, 255, 0.8)',
        },
        '& .MuiIconButton-root': {
          color: 'rgba(255, 255, 255, 0.8)',
        },
      },
      '&.Mui-focused': {
        '& .MuiOutlinedInput-notchedOutline': {
          borderColor: 'rgba(255, 255, 255, 0.8)',
        },
        '& .MuiIconButton-root': {
          color: 'rgba(255, 255, 255, 1)',
        },
      },
      '& .MuiIconButton-root': {
        color: 'rgba(255, 255, 255, 0.4)',
      },
    },
  };
}
