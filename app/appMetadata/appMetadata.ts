import { GridInitialStateCommunity } from '@mui/x-data-grid/models/gridStateCommunity';

export default class AppMetadata {
  static tableInitialState: GridInitialStateCommunity = {
    pagination: {
      paginationModel: {
        pageSize: 20,
      },
    },
  };
  static tableSizeOptions: number[] = [10, 20, 50, 100];
}
