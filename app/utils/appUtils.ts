import moment from "moment";

export default class AppUtils {
  static stringToColor = (string: string) => {
    let hash = 0;
    let i;

    for (i = 0; i < string.length; i += 1) {
      hash = string.charCodeAt(i) + ((hash << 5) - hash);
    }

    let color = '#';

    for (i = 0; i < 3; i += 1) {
      const value = (hash >> (i * 8)) & 0xff;
      color += `00${value.toString(16)}`.slice(-2);
    }

    return color;
  };

  static stringAvatar = (name: string) => {
    if (name) {
      return name.search(' ') >= 0
        ? `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`
        : name[0] + name[1];
    }
    return '';
  };

  static dateFormat(date: string) {
    return moment.utc(date).format('DD-MM-YYYY');
  }

  static dateTimeFormat(date: string) {
    return moment.utc(date).format('DD-MM-YYYY HH:mm:ss');
  }

  static isTimeInRange(timeNow:any, time:any, duration:any):{data: any, isTimeInRange: boolean} {
    // Chuyển đổi thời gian thành giây
    const timeASeconds = AppUtils.timeStringToSeconds(timeNow);
    const timeSeconds = AppUtils.timeStringToSeconds(time);
    const durationSeconds = duration * 60; // Chuyển duration từ phút sang giây
  
    // Kiểm tra xem timeA có nằm trong khoảng time và time + duration hay không
    if (timeASeconds >= timeSeconds && timeASeconds <= timeSeconds + durationSeconds) {
      return {data: time, isTimeInRange: true};
    } else {
      return {data: time, isTimeInRange: false};
    }
  }
  
  static timeStringToSeconds(timeString:any) {
    const [hours, minutes, seconds] = timeString.split(':').map(Number);
    return hours * 3600 + minutes * 60 + seconds;
  }
}