'use client';

import ApiDictionaries from '@/_api/api-dictionaries';
import axiosClient from '@/_api/axios-client';
import { validate } from 'email-validator';
import { useTranslations } from 'next-intl';
import { redirect, useRouter } from 'next/navigation';
import { ChangeEvent, FormEvent, useEffect } from 'react';
import { useSignal } from 'signals-react-safe';

type LoginForm = {
  email: string;
  password: string;
};

export default function useLogin() {
  const t = useTranslations();
  const router = useRouter();

  const loginForm = useSignal<LoginForm>({ email: '', password: '' });
  const errorText = useSignal<string>('');
  const isLoading = useSignal<boolean>(false);

  useEffect(() => {
    const jwt = sessionStorage.getItem('jwt');
    if (jwt) {
      redirect('/main/users');
    }
  }, []);

  const onLogin = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!validate(loginForm.value.email)) {
      errorText.value = t('emailInvalidate');
    } else {
      isLoading.value = true;
      try {
        const data: any = await axiosClient.post(ApiDictionaries.LOGIN(), {
          identifier: loginForm.value.email,
          password: loginForm.value.password,
        });

        if (data && data.jwt) {
          sessionStorage.setItem('jwt', data.jwt);
          router.replace('/main/users');
        } else {
          errorText.value = t('loginError');
        }
      } catch (error) {
        console.error(error);
      } finally {
        isLoading.value = false;
      }
    }
  };

  const onFormInput = (e: ChangeEvent<HTMLInputElement>) => {
    if (errorText) {
      errorText.value = '';
    }
    loginForm.value[e.target.name as keyof LoginForm] = e.target.value;
  };

  return { t, errorText, isLoading, onLogin, onFormInput };
}
