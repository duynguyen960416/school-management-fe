'use client';

import TopImage from '@/assets/images/auth_image.png';
import AppButton from '@/components/appButton';
import AppTextField from '@/components/appTextField';
import AppMaterial from '@/material/appMaterial';
import { Stack, Typography } from '@mui/material';
import Image from 'next/image';
import useLogin from './_useLogin';

export default function LoginPage() {
  const { t, isLoading, errorText, onFormInput, onLogin } = useLogin();
  
  return (
    <Stack
      sx={{
        m: '100px auto',
        px: AppMaterial.spacing.small,
        pt: AppMaterial.spacing.medium,
        pb: AppMaterial.spacing.large,
        width: 400,
        alignItems: 'center',
        backgroundColor: AppMaterial.colors.background1,
        borderRadius: 2,
      }}
    >
      <Image priority src={TopImage} alt='60th' width={350} />

      <Stack
        component='form'
        onSubmit={onLogin}
        width='100%'
        pt={AppMaterial.spacing.verySmall}
      >
        <AppTextField
          name='email'
          label={t('email')}
          sx={{ mt: AppMaterial.spacing.small }}
          required
          onChange={onFormInput}
        />
        <AppTextField
          name='password'
          label={t('password')}
          sx={{ mt: AppMaterial.spacing.verySmall }}
          required
          onChange={onFormInput}
          type='password'
        />
        <Typography
          color={AppMaterial.colors.error}
          fontSize={AppMaterial.fontSize.small}
          mt={AppMaterial.spacing.verySmall}
          textAlign='center'
          height={12}
        >
          {errorText}
        </Typography>
        <AppButton
          sx={{ mt: AppMaterial.spacing.small }}
          type='submit'
          signalloading={isLoading}
        >
          {t('login')}
        </AppButton>
      </Stack>
    </Stack>
  );
}
