'use client';

import { IconButton, InputAdornment, Stack } from "@mui/material";
import useCheckIn from "./_useCheckIn";
import AppTextField from "@/components/appTextField";
import { AddRounded, DeleteOutlineRounded, SearchRounded } from "@mui/icons-material";
import AppButton from "@/components/appButton";
import AppMaterial from "@/material/appMaterial";
import AppDrawer from "@/components/appDrawer";
import CheckinTableWidget from "@/views/main/checkin/checkinTableWidget";
import CheckOutTableWidget from "@/views/main/checkout/checkOutTableWidget";

export default function CheckInScreen() {
  const {
    t,
    rowSelectedValue,
    openDrawer,
    searchTextValue,
    onSearch,
    toggleDrawer,
    onCellClick,
    onItemSelection,
    user,
  } = useCheckIn();

  return  user.isAdmin && (
  <Stack>
      <Stack direction='row' justifyContent='space-between'>
        <AppTextField
          label={t('search')}
          onChange={onSearch}
          InputProps={{
            endAdornment: (
              <InputAdornment position='end'>
                <IconButton edge='end'>
                  <SearchRounded />
                </IconButton>
              </InputAdornment>
            ),
          }}
          sx={{ width: 300 }}
        />
      </Stack>
      <CheckinTableWidget onCellClick={onCellClick} onItemSelection={onItemSelection} searchText={searchTextValue}/>
     {}
    </Stack>
  );
}
