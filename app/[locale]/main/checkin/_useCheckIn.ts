import { currentUser } from '@/store/appStore';
import { GridCellParams, GridTreeNode } from '@mui/x-data-grid';
import dayjs from 'dayjs';
import { useAtom } from 'jotai';
import { useTranslations } from 'next-intl';
import { ChangeEvent } from 'react';
import { useSignal, useSignalAndValue } from 'signals-react-safe';

export default function useCheckIn() {
  const t = useTranslations();
  const [rowSelected, rowSelectedValue] = useSignalAndValue<number[]>([]);
  const [searchText, searchTextValue] = useSignalAndValue<string>('');
  const [user] = useAtom(currentUser);
  const [checkInField, checkInFieldValue] = useSignalAndValue<any>({
    user:'',
    dateTime: dayjs(),
  });

  const openDrawer = useSignal<boolean>(false);
  const itemSelected = useSignal<UserModel | null>(null);

  const onSearch = (e: ChangeEvent<HTMLInputElement>) => { 
    searchText.value = e.target.value;
  };


  const toggleDrawer = (value: boolean) => {
    if (!value && itemSelected.value != null) {
      checkInField.value = {
        user:'',
        department:0,
        dateTime: dayjs(),
      };
      itemSelected.value = null;
    }
    openDrawer.value = value;
  };

  const onCellClick = (
    item: GridCellParams<UserModel, unknown, unknown, GridTreeNode>
  ) => {
    if (item.field !== '__check__') {
      itemSelected.value = item.row;
      openDrawer.value = true;
    }
  };

  const onItemSelection = (e: any) => {
    rowSelected.value = e;
  };

  return {
    t,
    rowSelectedValue,
    openDrawer,
    user,
    onSearch,
    toggleDrawer,
    onItemSelection,
    onCellClick,
    searchTextValue,
  };
}
