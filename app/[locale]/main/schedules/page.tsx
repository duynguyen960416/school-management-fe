'use client';

import AppButton from '@/components/appButton';
import AppDatePicker from '@/components/appDatePicker';
import AppDrawer from '@/components/appDrawer';
import AppTextField from '@/components/appTextField';
import AppTimePicker from '@/components/appTimePicker';
import AppMaterial from '@/material/appMaterial';
import ScheduleTableWidget from '@/views/main/schedules/scheduleTableWidget';
import { AddRounded } from '@mui/icons-material';
import { MenuItem, Stack, Typography } from '@mui/material';
import dayjs from 'dayjs';
import useSchedules from './_useSchedules';
import moment from 'moment';
import AppDialog from '@/components/appDialog';

export default function SchedulesScreen() {
  const {
    t,
    openDrawer,
    createScheduleFormField,
    classes,
    weeks,
    subjects,
    departments,
    scheduleFieldValue,
    weekSelectedValue,
    scheduleSelectedValue,
    isSubmit,
    user,
    isRegistedValue,
    scheduleDialogItemValue,
    toggleDrawer,
    onCreateSchedule,
    onDateSelected,
    onSelectedOption,
    onTimeSelected,
    onWeekSelected,
    onScheduleItemClick,
    onRegistedSelect,
    onRegisterSchedule,
    onCloseRegisterDialog,
  } = useSchedules();

  return (
    <Stack>
      <Stack direction='row' justifyContent='space-between'>
        <AppTextField
          label={t('selectedWeek')}
          select
          SelectProps={{
            multiple: false,
            value: weekSelectedValue?.id ?? '',
            onChange: onWeekSelected,
          }}
          sx={{
            '& svg': { color: 'rgba(255, 255, 255, 0.4)' },
            width: 300,
          }}
        >
          {weeks.map((option: any, optionIndex: number) => {
            const isCurrentWeek = moment(option.startDate, 'DD-MM-YYYY').isSame(
              moment.utc(),
              'week'
            );
            return (
              <MenuItem
                key={optionIndex}
                value={option.id}
                sx={{
                  '&.Mui-selected': {
                    backgroundColor: 'primary.main',
                    '&:hover': {
                      backgroundColor: 'primary.main',
                    },
                  },
                }}
              >
                {`${option.name} ${
                  isCurrentWeek ? `(${t('currentWeek')})` : ''
                }`}
              </MenuItem>
            );
          })}
        </AppTextField>
        <Stack direction='row'>
          {user.isAdmin ? (
            <AppButton
              startIcon={<AddRounded />}
              onClick={() => toggleDrawer(true)}
              sx={{ ml: AppMaterial.spacing.verySmall }}
            >
              {t('add')}
            </AppButton>
          ) : (
            <AppButton
              onClick={onRegistedSelect}
              sx={{ ml: AppMaterial.spacing.verySmall }}
            >
              {t(isRegistedValue ? 'all' : 'registed')}
            </AppButton>
          )}
        </Stack>
      </Stack>
      <ScheduleTableWidget
        data={scheduleSelectedValue}
        onClick={onScheduleItemClick}
      />
      <AppDrawer
        title={t('createNewSchedule')}
        button={[
          {
            label: t('save'),
            type: 'submit',
            form: 'create-schedule',
            loading: isSubmit,
          },
        ]}
        open={openDrawer}
        onClose={() => toggleDrawer(false)}
        formId='create-schedule'
        onSubmit={onCreateSchedule}
      >
        {createScheduleFormField.map((el, index) => {
          if (el.type == 'date') {
            return el.name == 'date' ? (
              <AppDatePicker
                key={index}
                label={t(el.title)}
                value={dayjs(scheduleFieldValue.date)}
                slotProps={{ textField: { required: el.isRequired } }}
                minDate={dayjs()}
                onChange={onDateSelected}
              />
            ) : (
              <AppTimePicker
                key={index}
                label={t(el.title)}
                value={dayjs(scheduleFieldValue.time)}
                slotProps={{ textField: { required: el.isRequired } }}
                minTime={dayjs().set('hour', 7).set('minute', 0)}
                maxTime={dayjs().set('hour', 17).set('minute', 0)}
                onChange={onTimeSelected}
              />
            );
          }
          if (el.type == 'select') {
            let options: any = [];

            switch (el.name) {
              case 'subject':
                options = subjects;
                break;
              case 'class':
                options = classes;
                break;
              case 'week':
                options = weeks;
                break;
              case 'department':
                options = departments;
                break;
              case 'duration':
                options = [
                  {
                    id: 15,
                    name: t('15minutes'),
                  },
                  {
                    id: 30,
                    name: t('30minutes'),
                  },
                  {
                    id: 45,
                    name: t('45minutes'),
                  },
                  {
                    id: 60,
                    name: t('60minutes'),
                  },
                  {
                    id: 75,
                    name: t('75minutes'),
                  },
                  {
                    id: 90,
                    name: t('90minutes'),
                  },
                  {
                    id: 105,
                    name: t('105minutes'),
                  },
                  {
                    id: 120,
                    name: t('120minutes'),
                  },
                ];
                break;
            }

            return (
              <AppTextField
                key={index}
                label={t(el.title)}
                name={el.name}
                select
                required
                SelectProps={{
                  multiple: false,
                  value: scheduleFieldValue[el.name],
                  onChange: onSelectedOption,
                }}
                sx={{
                  mb: AppMaterial.spacing.verySmall,
                  '& svg': { color: 'rgba(255, 255, 255, 0.4)' },
                }}
              >
                {options.map((option: any, optionIndex: number) => (
                  <MenuItem
                    key={optionIndex}
                    value={option.id}
                    sx={{
                      '&.Mui-selected': {
                        backgroundColor: 'primary.main',
                        '&:hover': {
                          backgroundColor: 'primary.main',
                        },
                      },
                    }}
                  >
                    {option.name}
                  </MenuItem>
                ))}
              </AppTextField>
            );
          }
        })}
      </AppDrawer>
      <AppDialog
        open={scheduleDialogItemValue != null}
        title={t('courseRegistration')}
        content={
          <Stack width={250}>
            <Stack direction='row' justifyContent='space-between'>
              <Typography sx={{ color: AppMaterial.colors.warning }}>
                {t('subject')}:
              </Typography>
              <Typography>{scheduleDialogItemValue?.subjectName}</Typography>
            </Stack>
            <Stack direction='row' justifyContent='space-between'>
              <Typography sx={{ color: AppMaterial.colors.warning }}>
                {t('room')}:
              </Typography>
              <Typography>{scheduleDialogItemValue?.room}</Typography>
            </Stack>
            <Stack direction='row' justifyContent='space-between'>
              <Typography sx={{ color: AppMaterial.colors.warning }}>
                {t('credit')}:
              </Typography>
              <Typography>{scheduleDialogItemValue?.credit}</Typography>
            </Stack>
            <Stack direction='row' justifyContent='space-between'>
              <Typography sx={{ color: AppMaterial.colors.warning }}>
                {t('duration')}:
              </Typography>
              <Typography>{`${scheduleDialogItemValue?.duration} ${t('min')}`}</Typography>
            </Stack>
            <Stack direction='row' justifyContent='space-between'>
              <Typography sx={{ color: AppMaterial.colors.warning }}>
                {t('startTime')}:
              </Typography>
              <Typography>{dayjs(scheduleDialogItemValue?.startTime, 'HH:mm').format('HH:mm')}</Typography>
            </Stack>
          </Stack>
        }
        actionTitle={t('register')}
        onSubmit={onRegisterSchedule}
        handleClose={onCloseRegisterDialog}
        actionDisabled={scheduleDialogItemValue?.isRegisted}
      />
    </Stack>
  );
}
