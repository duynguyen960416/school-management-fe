import ApiDictionaries from '@/_api/api-dictionaries';
import axiosClient from '@/_api/axios-client';
import {
  classesList,
  currentUser,
  departmentList,
  scheduleList,
  subjectList,
  weekList,
} from '@/store/appStore';
import AppUtils from '@/utils/appUtils';
import dayjs from 'dayjs';
import { useAtom } from 'jotai';
import _, { set, update } from 'lodash';
import moment from 'moment';
import { useTranslations } from 'next-intl';
import { useEffect } from 'react';
import { useSignal, useSignalAndValue } from 'signals-react-safe';

interface ScheduleDialogModel {
  scheduleId: number;
  subjectName: string;
  room: string;
  credit: number;
  duration: number;
  startTime: string;
  isRegisted: boolean;
}

const createScheduleFormField: AppFieldType[] = [
  {
    title: 'week',
    name: 'week',
    type: 'select',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'subject',
    name: 'subject',
    type: 'select',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'class',
    name: 'class',
    type: 'select',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'department',
    name: 'department',
    type: 'select',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'date',
    name: 'date',
    type: 'date',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'time',
    name: 'time',
    type: 'date',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'duration',
    name: 'duration',
    type: 'select',
    canEdit: true,
    isRequired: true,
  },
];

export default function useSchedules() {
  const t = useTranslations();
  const itemSelected = useSignal<ScheduleModel | null>(null);
  const openDrawer = useSignal<boolean>(false);
  const [user, setUser] = useAtom(currentUser);
  const [schedules, setSchedules] = useAtom(scheduleList);
  const [subjects, setSubjects] = useAtom(subjectList);
  const [classes, setClasses] = useAtom(classesList);
  const [weeks, setWeeks] = useAtom(weekList);
  const [departments, setDepartments] = useAtom(departmentList);
  const [scheduleField, scheduleFieldValue] = useSignalAndValue<any>({
    subject: '',
    date: dayjs(),
    time: dayjs(),
    class: '',
    week: '',
    department: '',
    duration: '',
  });
  const [weekSelected, weekSelectedValue] = useSignalAndValue<WeekModel | null>(
    null
  );
  const [scheduleSelected, scheduleSelectedValue] = useSignalAndValue<
    ScheduleModel[]
  >([]);
  const isSubmit = useSignal<boolean>(false);
  const [isRegisted, isRegistedValue] = useSignalAndValue<boolean>(false);
  const [scheduleDialogItem, scheduleDialogItemValue] =
    useSignalAndValue<ScheduleDialogModel | null>(null);

  useEffect(() => {
    const currentWeek = weeks.find((week) =>
      moment(week.startDate, 'DD-MM-YYYY').isSame(moment.utc(), 'week')
    );
    const currentSchedule = schedules.filter(
      (schedule) => schedule.week === currentWeek?.id
    );
    weekSelected.value = currentWeek ?? weeks[0];
    scheduleSelected.value = currentSchedule ?? [];

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [weeks, schedules]);

  const toggleDrawer = (value: boolean) => {
    if (!value && itemSelected.value != null) {
      scheduleField.value = {
        subject: '',
        date: dayjs(),
        time: dayjs(),
        class: '',
        week: '',
        department: '',
        duration: '',
      };
      itemSelected.value = null;
    }
    openDrawer.value = value;
  };

  const onCreateSchedule = async (e: any) => {
    e.preventDefault();
    isSubmit.value = true;
    try {
      if (itemSelected.value) {
        await updateSchedule();
      } else {
        await createNewSchedule();
      }
    } catch (error) {
    } finally {
      openDrawer.value = false;
      isSubmit.value = false;
    }
  };

  const createNewSchedule = async () => {
    const data: any = await axiosClient.post(ApiDictionaries.SCHEDULE(), {
      data: {
        date: dayjs(scheduleFieldValue.date).format('YYYY-MM-DD'),
        time: dayjs(scheduleFieldValue.time).format('HH:mm:ss'),
        subject: scheduleFieldValue.subject,
        class: scheduleFieldValue.class,
        week: scheduleFieldValue.week,
        department: scheduleFieldValue.department,
        duration: scheduleFieldValue.duration,
      },
    });

    if (data) {
      const newSchedule = {
        date: AppUtils.dateFormat(data.data.attributes.date),
        time: data.data.attributes.time,
        week: data.data.attributes.week.data.id,
        duration: data.data.attributes.duration,
        subject: data.data.attributes.subject.data.id,
        users:
          data.data.attributes.users.data.map((user: any) => user.id) ?? [],
        checkIn: data.data.attributes.check_ins.data.map(
          (checkIn: any) => checkIn.id
        ),
        checkOut: data.data.attributes.check_outs.data.map(
          (checkOut: any) => checkOut.id
        ),
        class: data.data.attributes.class.data.id,
        department: data.data.attributes.department.data.id,
      };
      const scheduleClone = _.cloneDeep(schedules);
      scheduleClone.unshift(newSchedule);
      setSchedules(scheduleClone);
    }
  };

  const updateSchedule = async () => {
    const data: any = await axiosClient.put(
      ApiDictionaries.SCHEDULE(itemSelected.value?.id?.toString()),
      {
        data: {
          date: dayjs(scheduleFieldValue.date).format('YYYY-MM-DD'),
          time: dayjs(scheduleFieldValue.time).format('HH:mm:ss'),
          subject: scheduleFieldValue.subject,
          class: scheduleFieldValue.class,
          week: scheduleFieldValue.week,
          department: scheduleFieldValue.department,
          duration: scheduleFieldValue.duration,
        },
      }
    );

    if (data) {
      const scheduleClone = _.cloneDeep(schedules);
      const index = scheduleClone.findIndex(
        (schedule) => schedule.id === itemSelected.value?.id
      );
      if (index !== -1) {
        const newSchedule = {
          id: data.data.id,
          date: AppUtils.dateFormat(data.data.attributes.date),
          time: data.data.attributes.time,
          week: data.data.attributes.week.data.id,
          duration: data.data.attributes.duration,
          subject: data.data.attributes.subject.data.id,
          users:
            data.data.attributes.users.data.map((user: any) => user.id) ?? [],
          checkIn: data.data.attributes.check_ins.data.map(
            (checkIn: any) => checkIn.id
          ),
          checkOut: data.data.attributes.check_outs.data.map(
            (checkOut: any) => checkOut.id
          ),
          class: data.data.attributes.class.data.id,
          department: data.data.attributes.department.data.id,
        };
        scheduleClone[index] = newSchedule;
        setSchedules(scheduleClone);
      }
    }
  };

  const onDateSelected = (e: any) => {
    scheduleField.value.date = e;
  };

  const onTimeSelected = (e: any) => {
    scheduleField.value.time = e;
  };

  const onSelectedOption = (e: any) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    const scheduleClone = { ...scheduleFieldValue };
    scheduleClone[fieldName] = fieldValue;
    scheduleField.value = scheduleClone;
  };

  const onWeekSelected = (e: any) => {
    const newWeekSelected = weeks.find((week) => week.id === e.target.value);
    weekSelected.value = newWeekSelected ?? null;
    const currentSchedule = schedules.filter(
      (schedule) => schedule.week === e.target.value
    );
    scheduleSelected.value = currentSchedule ?? [];
  };

  const onScheduleItemClick = (item: ScheduleModel) => {
    if (user.isAdmin) {
      scheduleField.value = {
        subject: item.subject,
        date: dayjs(item.date, 'DD-MM-YYYY'),
        time: dayjs(item.time, 'HH:mm:ss'),
        class: item.class,
        week: item.week,
        department: item.department,
        duration: item.duration,
      };
      itemSelected.value = item;
      toggleDrawer(true);
    } else {
      const subject = subjects.find((el) => el.id == item?.subject);
      const department = departments.find((el) => el.id == item?.department);
      scheduleDialogItem.value = {
        scheduleId: item.id!,
        subjectName: subject?.name ?? '',
        room: department?.name ?? '',
        credit: subject?.credit ?? 0,
        duration: item.duration,
        startTime: item.time,
        isRegisted: item.users.includes(user.id!),
      };
    }
  };

  const onRegistedSelect = () => {
    if (isRegistedValue) {
      const currentSchedule = schedules.filter(
        (schedule) => schedule.week === weekSelectedValue?.id
      );
      scheduleSelected.value = currentSchedule ?? [];
      isRegisted.value = false;
    } else {
      const newSchedule = scheduleSelectedValue.filter((el) =>
        el.users.includes(user.id!)
      );
      scheduleSelected.value = newSchedule;
      isRegisted.value = true;
    }
  };

  const onCloseRegisterDialog = () => {
    scheduleDialogItem.value = null;
  };

  const onRegisterSchedule = async () => {
    const data: any = await axiosClient.put(
      ApiDictionaries.USER(user?.id?.toString()),
      {
        schedules: [...user?.schedules, scheduleDialogItemValue?.scheduleId],
      }
    );
    if (data) {
      const userClone = _.cloneDeep(user);
      const scheduleClone = _.cloneDeep(schedules);
      const scheduleIndex = scheduleClone.findIndex(
        (el) => el.id === scheduleDialogItemValue?.scheduleId
      );

      if (scheduleIndex !== -1) {
        scheduleClone[scheduleIndex].users.push(user.id!);
        setSchedules(scheduleClone);
      }

      userClone.schedules = [
        ...userClone.schedules!,
        scheduleDialogItemValue!.scheduleId!,
      ];
      setUser(userClone);
    }
  };

  return {
    t,
    openDrawer,
    createScheduleFormField,
    classes,
    weeks,
    subjects,
    departments,
    scheduleFieldValue,
    weekSelectedValue,
    scheduleSelectedValue,
    isSubmit,
    user,
    isRegistedValue,
    scheduleDialogItemValue,
    toggleDrawer,
    onCreateSchedule,
    onDateSelected,
    onSelectedOption,
    onTimeSelected,
    onWeekSelected,
    onScheduleItemClick,
    onRegistedSelect,
    onRegisterSchedule,
    onCloseRegisterDialog,
  };
}
