'use client';

import AppButton from '@/components/appButton';
import AppDrawer from '@/components/appDrawer';
import AppTextField from '@/components/appTextField';
import AppMaterial from '@/material/appMaterial';
import {
  AddRounded,
  DeleteOutlineRounded,
  SearchRounded,
} from '@mui/icons-material';
import { IconButton, InputAdornment, MenuItem, Stack } from '@mui/material';
import useDepartments from './_useDepartments';
import DepartmentsTableWidget from '@/views/main/departments/departmentsTableWidget';

export default function DepartmentsScreen() {
  const {
    t,
    rowSelectedValue,
    openDrawer,
    createDepartmentFormField,
    departmentFieldValue,
    searchTextValue,
    isSubmit,
    user,
    onSearch,
    toggleDrawer,
    onCreateDepartment,
    onFieldChange,
    onIsClassChange,
    onCellClick,
    onItemSelection,
  } = useDepartments();

  return user.isAdmin && (
     <Stack>
      <Stack direction='row' justifyContent='space-between'>
        <AppTextField
          label={t('search')}
          onChange={onSearch}
          InputProps={{
            endAdornment: (
              <InputAdornment position='end'>
                <IconButton edge='end'>
                  <SearchRounded />
                </IconButton>
              </InputAdornment>
            ),
          }}
          sx={{ width: 300 }}
        />
        <Stack direction='row'>
          {rowSelectedValue.length > 0 && (
            <AppButton
              startIcon={<DeleteOutlineRounded />}
              // onClick={onDeleteDepartment}
              color='error'
            >
              {t('delete')}
            </AppButton>
          )}
          <AppButton
            startIcon={<AddRounded />}
            onClick={() => toggleDrawer(true)}
            sx={{ ml: AppMaterial.spacing.verySmall }}
          >
            {t('add')}
          </AppButton>
        </Stack>
      </Stack>
      <DepartmentsTableWidget
        onCellClick={onCellClick}
        onItemSelection={onItemSelection}
        searchText={searchTextValue}
      />
      <AppDrawer
        title={'Create Department'}
        button={[
          {
            label: t('save'),
            type: 'submit',
            form: 'create-department',
            loading: isSubmit,
          },
        ]}
        open={openDrawer}
        onClose={() => toggleDrawer(false)}
        formId='create-department'
        onSubmit={onCreateDepartment}
      >
        {createDepartmentFormField.map((el, index) => {
          if (el.type == 'text' || el.type == 'number') {
            return (
              <AppTextField
                key={index}
                defaultValue={departmentFieldValue[el.name as keyof DepartmentModel] ?? null}
                name={el.name}
                label={t(el.title)}
                required={el.isRequired}
                sx={{ mb: AppMaterial.spacing.verySmall }}
                type={el.type}
                onChange={onFieldChange}
              />
            );
          }
          if (el.type == 'select') {
            return (
              <AppTextField
                key={index}
                label={t(el.title)}
                select
                required
                SelectProps={{
                  multiple: false,
                  value: departmentFieldValue.isClass,
                  onChange: onIsClassChange,
                }}
                sx={{
                  mb: AppMaterial.spacing.verySmall,
                  '& svg': { color: 'rgba(255, 255, 255, 0.4)' },
                }}
              >
                {['yes', 'no'].map((isClass, isClassIndex) => (
                  <MenuItem
                    key={isClassIndex}
                    value={isClass}
                    sx={{
                      '&.Mui-selected': {
                        backgroundColor: 'primary.main',
                        '&:hover': {
                          backgroundColor: 'primary.main',
                        },
                      },
                    }}
                  >
                    {t(isClass)}
                  </MenuItem>
                ))}
              </AppTextField>
            );
          }
        })}
      </AppDrawer>
    </Stack>
  );
}
