import ApiDictionaries from '@/_api/api-dictionaries';
import axiosClient from '@/_api/axios-client';
import { GridCellParams, GridTreeNode } from '@mui/x-data-grid';
import { useTranslations } from 'next-intl';
import { useRouter } from 'next/navigation';
import { ChangeEvent, FormEvent, useEffect } from 'react';
import { useSignal, useSignalAndValue } from 'signals-react-safe';
import { useAtom } from 'jotai';
import { currentUser, departmentList } from '@/store/appStore';
import _ from 'lodash';

const createDepartmentFormField: AppFieldType[] = [
  {
    title: 'name',
    name: 'name',
    type: 'text',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'capacity',
    name: 'capacity',
    type: 'number',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'class',
    name: 'isClass',
    type: 'select',
    canEdit: true,
    isRequired: true,
  },
];

export default function useDepartments() {
  const t = useTranslations();
  const router = useRouter();
  const [user] = useAtom(currentUser);
  const [department, setDepartments] = useAtom(departmentList)
  const [rowSelected, rowSelectedValue] = useSignalAndValue<number[]>([]);
  const openDrawer = useSignal<boolean>(false);
  const [departmentField, departmentFieldValue] = useSignalAndValue<any>({
    name: '',
    capacity: 0,
    isClass: 'no',
  });
  const [searchText, searchTextValue] = useSignalAndValue<string>('');
  const [itemSelected, itemSelectedValue] = useSignalAndValue<DepartmentModel | null>(
    null
  );
  const isSubmit = useSignal<boolean>(false);

  useEffect(() => {
    if (user.studentCode && !user.isAdmin) {
      router.replace('/main/schedules');
    }
  }, [router, user]);

  const onSearch = (e: ChangeEvent<HTMLInputElement>) => { 
    searchText.value = e.target.value;
  };

  const onDeleteDepartment = () => { };

  const toggleDrawer = (value: boolean) => {
    if (!value && itemSelected.value != null) {
      departmentField.value = {
        name: '',
        capacity: 0,
        isClass: 'no',
      };
      itemSelected.value = null;
    }
    openDrawer.value = value;
  };

  const onFieldChange = (e: ChangeEvent<HTMLInputElement>) => {
    const fieldName = e.target.name;

    if (
      fieldName == 'name' ||
      fieldName == 'capacity' ||
      fieldName == 'isClass'
    ) {
      departmentField.value[fieldName as keyof DepartmentModel] = e.target.value;
    }
  };

  const onCreateDepartment = async (e: FormEvent) => {
    e.preventDefault();
    isSubmit.value = true;
    try {
      if (itemSelectedValue) {
        await updateDepartment();
      }
      else{
      await createNewDepartment();
    }
    } catch (error) {
      console.log(error)
    } finally {
      isSubmit.value = false;
      openDrawer.value = false;
    }
  };

  const createNewDepartment = async () => {

    const req = await axiosClient.post(ApiDictionaries.DEPARTMENT(),
      {
        data:
        {
          ...departmentFieldValue,
          capacity: parseInt(departmentFieldValue.capacity),
          isClass: departmentFieldValue.isClass == 'yes'
        }
      });
    const departmentClone = _.cloneDeep(department);
    const newDepartment = {
      id: req.data.id,
      name: req.data.attributes.name,
      capacity: req.data.attributes.capacity,
      isClass: req.data.attributes.isClass,
    }
    departmentClone.unshift(newDepartment)
    setDepartments(departmentClone);
  };

  const updateDepartment = async () => {
    const data: any = await axiosClient.put(
      ApiDictionaries.DEPARTMENT(itemSelectedValue?.id?.toString()),
      {
        data: {
          ...departmentFieldValue,
          capacity: parseInt(departmentFieldValue.capacity),
          isClass: departmentFieldValue.isClass == 'yes'
        },
      }
    );
    if (data) {
      const departmentClone = _.cloneDeep(department);
      const departmentIndex = departmentClone.findIndex(
        (item) => item.id === data.data.id
      );
      if (departmentIndex > -1) {
        const newDepartment = {
          id: data.data.id,
          name: data.data.attributes.name,
          capacity: data.data.attributes.capacity,
          isClass: data.data.attributes.isClass,
        }
        
        departmentClone[departmentIndex] = newDepartment;
        setDepartments(departmentClone);
        openDrawer.value = false;

      }
    }
  }

  const onIsClassChange = (e: any) => {
    const departmentFieldClone = { ...departmentField.value };
    const fieldValue = e.target.value;
    departmentFieldClone.isClass = fieldValue;
    departmentField.value = departmentFieldClone;
  };

  const onCellClick = (
    item: GridCellParams<DepartmentModel, unknown, unknown, GridTreeNode>
  ) => {
    if (item.field !== '__check__') {
      departmentField.value = {
        name: item.row.name,
        capacity: item.row.capacity,
        isClass: item.row.isClass ? 'yes' : 'no',
      }
      itemSelected.value = item.row;

      openDrawer.value = true;
    }
  };

  const onItemSelection = (e: any) => {
    rowSelected.value = e;
  };

  return {
    t,
    rowSelectedValue,
    openDrawer,
    createDepartmentFormField,
    departmentFieldValue,
    isSubmit,
    user,
    searchTextValue,
    onSearch,
    toggleDrawer,
    onCreateDepartment,
    onFieldChange,
    onIsClassChange,
    onItemSelection,
    onCellClick,
  };
}
