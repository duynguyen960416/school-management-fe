import ApiDictionaries from '@/_api/api-dictionaries';
import axiosClient from '@/_api/axios-client';
import { checkInList, checkOutList, classesList, currentUser, departmentList, rfidCardList, scheduleList, subjectList, userList, weekList } from '@/store/appStore';
import AppUtils from '@/utils/appUtils';
import { useAtom } from 'jotai';
import { set } from 'lodash';
import moment from 'moment';
import { redirect } from 'next/navigation';
import { useEffect } from 'react';

export default function useMainLayout() {
  const [_currentUser, setCurrentUser] = useAtom(currentUser);
  const [_userList, setUserList] = useAtom(userList);
  const [_departmentList, setDepartmentList] = useAtom(departmentList);
  const [_classList, setClassList] = useAtom(classesList);
  const [_scheduleList, setScheduleList] = useAtom(scheduleList);
  const [_subjectList, setSubjectList] = useAtom(subjectList);
  const [_rfidCardList, setRfidCardList] = useAtom(rfidCardList);
  const [_weekList, setWeekList] = useAtom(weekList);
  const [_checkInList, setCheckInList] = useAtom(checkInList);
  const [_checkOutList, setCheckOutList] = useAtom(checkOutList);

  useEffect(() => {
    async function getProfile() {
      Promise.all([
        axiosClient.get(ApiDictionaries.PROFILE()),
        axiosClient.get(ApiDictionaries.USER()),
        axiosClient.get(ApiDictionaries.DEPARTMENT()),
        axiosClient.get(ApiDictionaries.CLASSES()),
        axiosClient.get(ApiDictionaries.SCHEDULE()),
        axiosClient.get(ApiDictionaries.SUBJECT()),
        axiosClient.get(ApiDictionaries.RFID_CARD()),
        axiosClient.get(ApiDictionaries.WEEK()),
        axiosClient.get(ApiDictionaries.CHECK_IN()),
        axiosClient.get(ApiDictionaries.CHECK_OUT()),
      ]).then((res) => {
        if (
          res[0] &&
          res[1] &&
          res[2] &&
          res[3] &&
          res[4] &&
          res[5] &&
          res[6] &&
          res[7] &&
          res[8] &&
          res[9]
        ) {
          const profileData: any = res[0];
          const userList: any = res[1];
          const departmentList: any = res[2];
          const classList: any = res[3];
          const scheduleList: any = res[4];
          const subjectList: any = res[5];
          const rfidCardList: any = res[6];
          const weekList: any = res[7];
          const checkInList: any = res[8];
          const checkOutList: any = res[9];
          const users = userList.map((el: any) => ({
            id: el.id,
            email: el.email,
            phoneNumber: el.phoneNumber,
            fullName: el.fullName,
            studentCode: el.studentCode,
            dob: AppUtils.dateFormat(el.dob),
            class: el.class ? el.class.id : null,
            address: el.address,
            cardId: el.cardId ? el.cardId.id : null,
            checkIn: el.check_ins ?? [],
            checkOut: el.check_outs ?? [],
            subjects: el.subjects ?? [],
            isAdmin: el.isAdmin ?? false,
            isStudent: el.isStudent ?? false,
            isTeacher: el.isTeacher ?? false,
            isActive: el.is_active ?? false,
            createdAt: el.createdAt,
          }));

          const departments = departmentList.data.map((el: any) => ({
            id: el.id,
            name: el.attributes.name,
            isClass: el.attributes.isClass,
            capacity: el.attributes.capacity,
          }));

          const classes = classList.data.map((el: any) => ({
            id: el.id,
            name: el.attributes.name,
            startDate: AppUtils.dateFormat(el.attributes.startDate),
            endDate: AppUtils.dateFormat(el.attributes.endDate),
            users: el.attributes.users.data.map((user: any) => user.id) ?? [],
            schedules: el.attributes.schedules.data.map((schedule: any) => schedule.id) ?? [],
          }));
          
          const schedules = scheduleList.data.map((el: any) => ({
            id: el.id,
            date: AppUtils.dateFormat(el.attributes.date),
            time: el.attributes.time,
            week: el.attributes.week.data.id,
            duration: el.attributes.duration,
            subject: el.attributes.subject.data.id,
            users: el.attributes.users.data.map((user: any) => user.id) ?? [],
            checkIn: el.attributes.check_ins.data.map((checkIn: any) => checkIn.id),
            checkOut: el.attributes.check_outs.data.map((checkOut: any) => checkOut.id),
            class: el.attributes.class.data?.id,
            department: el.attributes.department.data.id,
          }));


          const subjects = subjectList.data.map((el: any) => ({
            id: el.id,
            name: el.attributes.name,
            credit: el.attributes.credit,
            schedules: el.attributes.schedules.data.map((schedule: any) => schedule.id) ?? [],
          }));

          const rfidCards = rfidCardList.data.map((el: any) => ({
            id: el.id,
            cardId: el.attributes.cardId,
            user: el.attributes.user.data?.id,
          }));

          const weeks = weekList.data.map((el: any) => ({
            id: el.id,
            name: el.attributes.name,
            startDate: AppUtils.dateFormat(el.attributes.startDate),
            endDate: AppUtils.dateFormat(el.attributes.endDate),
            schedules: el.attributes.schedules.data.map((cl: any) => cl.id) ?? [],
          }));
          
          const checkIns = checkInList.data.map((el: any) => ({
            
            id: el.id,
            user: el.attributes.user.data?.id ?? null,
            department: el.attributes.department.data?.id ?? null,
            dateTime: AppUtils.dateTimeFormat(el.attributes.dateTime),
          }
          ));

          const checkOuts = checkOutList.data.map((el: any) => ({
            id: el.id,
            user: el.attributes.user.data?.id ?? null,
            department: el.attributes.department.data?.id ?? null,
            dateTime: AppUtils.dateTimeFormat(el.attributes.dateTime),
          }));

          users.sort(
            (a: UserModel, b: UserModel) =>
              moment(b.createdAt).unix() - moment(a.createdAt).unix()
          );
          const profile = users.find((el: any) => el.id === profileData.id);
          setCurrentUser(profile);
          setUserList(users);
          setDepartmentList(departments);
          setClassList(classes);
          setScheduleList(schedules);
          setSubjectList(subjects);
          setRfidCardList(rfidCards);
          setWeekList(weeks);
          setCheckInList(checkIns);
          setCheckOutList(checkOuts);
        } else {
          sessionStorage.clear();
          redirect('/login');
        }
      });
    }

    const jwt = sessionStorage.getItem('jwt');
    if (!jwt) {
      redirect('/login');
    } else {
      axiosClient.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
      getProfile();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { _currentUser };
}
