'use client';

import { IconButton, InputAdornment, Stack } from "@mui/material";
import AppTextField from "@/components/appTextField";
import { SearchRounded } from "@mui/icons-material";
import CheckinTableWidget from "@/views/main/checkin/checkinTableWidget";
import useCheckOut from "./_useCheckOut";
import CheckOutTableWidget from "@/views/main/checkout/checkOutTableWidget";

export default function CheckOutScreen() {
  const {
    t,
    rowSelectedValue,
    openDrawer,
    searchTextValue,
    user,
    onSearch,
    toggleDrawer,
    onCellClick,
    onItemSelection,
  } = useCheckOut();

  return user.isAdmin && ( 
  <Stack>
      <Stack direction='row' justifyContent='space-between'>
        <AppTextField
          label={t('search')}
          onChange={onSearch}
          InputProps={{
            endAdornment: (
              <InputAdornment position='end'>
                <IconButton edge='end'>
                  <SearchRounded />
                </IconButton>
              </InputAdornment>
            ),
          }}
          sx={{ width: 300 }}
        />
      </Stack>
      <CheckOutTableWidget onCellClick={onCellClick} onItemSelection={onItemSelection} searchText={searchTextValue}/>
     {}
    </Stack>
  );

  
}
