'use client';

import { redirect } from 'next/navigation';
import { useEffect } from 'react';

export default function MainPage() {
  useEffect(() => {
    const jwt = sessionStorage.getItem('jwt');
    if (!jwt) {
      redirect('/login');
    } else {
      redirect('/main/users');
    }
  }, []);
}
