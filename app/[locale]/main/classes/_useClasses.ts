import ApiDictionaries from '@/_api/api-dictionaries';
import axiosClient from '@/_api/axios-client';
import { GridCellParams, GridTreeNode } from '@mui/x-data-grid';
import { useTranslations } from 'next-intl';
import { useRouter } from 'next/navigation';
import { ChangeEvent, FormEvent, useEffect } from 'react';
import { useSignal, useSignalAndValue } from 'signals-react-safe';
import { useAtom } from 'jotai';
import { classesList, currentUser, departmentList } from '@/store/appStore';
import _ from 'lodash';
import dayjs from 'dayjs';

const createClassFormField: AppFieldType[] = [
  {
    title: 'name',
    name: 'name',
    type: 'text',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'startDate',
    name: 'startDate',
    type: 'date',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'endDate',
    name: 'endDate',
    type: 'date',
    canEdit: true,
    isRequired: true,
  },
];

export default function useClasses() {
  const t = useTranslations();
  const router = useRouter();
  const [user] = useAtom(currentUser);
  const [classes, setClasses] = useAtom(classesList)
  const [rowSelected, rowSelectedValue] = useSignalAndValue<number[]>([]);
  const openDrawer = useSignal<boolean>(false);
  const [classField, classFieldValue] = useSignalAndValue<any>({
    name: '',
    startDate: '',
    endDate: '',
  });
  const [searchText, searchTextValue] = useSignalAndValue<string>('');
  const [itemSelected, itemSelectedValue] = useSignalAndValue<ClassesModel | null>(
    null
  );
  const isSubmit = useSignal<boolean>(false);

  useEffect(() => {
    if (user.studentCode && !user.isAdmin) {
      router.replace('/main/schedules');
    }
  }, [router, user]);

  const onSearch = (e: ChangeEvent<HTMLInputElement>) => {
    searchText.value = e.target.value;
  };

  const onDeleteClass = () => { };

  const onStartDateSelected = (e: any) => {
    classField.value.startDate = e;
  };
  const onEndDateSelected = (e: any) => {
    classField.value.endDate = e;
  };
  const onDateSelected = (e: any) => {
    classField.value.startDate = e;
    classField.value.endDate = e;
  };


  const toggleDrawer = (value: boolean) => {
    if (!value && itemSelected.value != null) {
      classField.value = {
        name: '',
        startDate: '',
        endDate: '',
      };
      itemSelected.value = null;
    }
    openDrawer.value = value;
  };

  const onFieldChange = (e: ChangeEvent<HTMLInputElement>) => {
    const fieldName = e.target.name;

    if (
      fieldName == 'name' ||
      fieldName == 'startDate' ||
      fieldName == 'endDate'
    ) {
      classField.value[fieldName as keyof ClassesModel] = e.target.value;
    }
  };

  const onCreateClass = async (e: FormEvent) => {
    e.preventDefault();
    isSubmit.value = true;
    try {
      if (itemSelectedValue) {
        await updateClass();
      }
      else {
        await createNewClass();
      }
    } catch (error) {
      console.log(error)
    } finally {
      isSubmit.value = false;
      openDrawer.value = false;
    }
  };

  const createNewClass = async () => {
    const startDate = dayjs(classFieldValue.startDate).format('YYYY-MM-DD');
    const endDate = dayjs(classFieldValue.endDate).format('YYYY-MM-DD');
    const req = await axiosClient.post(ApiDictionaries.CLASSES(),
      {
        data:
        {
          ...classFieldValue,
          startDate: startDate,
          endDate: endDate,
          users: [],
          schedules: []
        }
      });
    const classClone = _.cloneDeep(classes);
    const newClass = {
      id: req.data.id,
      name: req.data.attributes.name,
      startDate: req.data.attributes.startDate,
      endDate: req.data.attributes.endDate,
      users: req.data.attributes.users,
      schedules: req.data.attributes.schedules,
    }
    classClone.unshift(newClass)
    setClasses(classClone);
  };

  const updateClass = async () => {
    const data: any = await axiosClient.put(
      ApiDictionaries.CLASSES(itemSelectedValue?.id?.toString()),
      {
        data: {
          ...classFieldValue,
          capacity: parseInt(classFieldValue.capacity),
          isClass: classFieldValue.isClass == 'yes'
        },
      }
    );
    if (data) {
      const classClone = _.cloneDeep(classes);
      const classIndex = classClone.findIndex(
        (item: any) => item.id === data.data.id
      );
      if (classIndex > -1) {
        const newClass = {
          id:data.id,
          name: data.attributes.name,
          startDate: data.attributes.startDate,
          endDate: data.attributes.endDate,
          users: data.attributes.users,
          schedules: data.attributes.schedules,
        }

        classClone[classIndex] = newClass;
        setClasses(classClone);
        openDrawer.value = false;

      }
    }
  }

  const onIsClassChange = (e: any) => {
    const classFieldClone = { ...classField.value };
    const fieldValue = e.target.value;
    classFieldClone.isClass = fieldValue;
    classField.value = classFieldClone;
  };

  const onCellClick = (
    item: GridCellParams<ClassesModel, unknown, unknown, GridTreeNode>
  ) => {
    if (item.field !== '__check__') {
      classField.value = {
        name: item.row.name,
        startDate: item.row.startDate,
        endDate: item.row.endDate
      }
      itemSelected.value = item.row;

      openDrawer.value = true;
    }
  };

  const onItemSelection = (e: any) => {
    rowSelected.value = e;
  };

  return {
    t,
    rowSelectedValue,
    openDrawer,
    createClassFormField,
    classFieldValue,
    isSubmit,
    user,
    searchTextValue,
    onSearch,
    onStartDateSelected,
    onEndDateSelected,
    onDateSelected,
    toggleDrawer,
    onCreateClass,
    onFieldChange,
    onIsClassChange,
    onItemSelection,
    onCellClick,
  };
}
