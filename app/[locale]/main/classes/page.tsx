'use client';

import AppButton from '@/components/appButton';
import AppDrawer from '@/components/appDrawer';
import AppTextField from '@/components/appTextField';
import AppMaterial from '@/material/appMaterial';
import DepartmentsTableWidget from '@/views/main/departments/departmentsTableWidget';
import {
  AddRounded,
  Class,
  DeleteOutlineRounded,
  SearchRounded,
} from '@mui/icons-material';
import { IconButton, InputAdornment, MenuItem, Stack } from '@mui/material';
import useClasses from './_useClasses';
import ClassTableWidget from '@/views/main/classes/classTableWidget';
import AppDatePicker from '@/components/appDatePicker';
import dayjs from 'dayjs';

export default function DepartmentsScreen() {
  const {
    t,
    rowSelectedValue,
    openDrawer,
    createClassFormField,
    classFieldValue,
    searchTextValue,
    isSubmit,
    user,
    onSearch,
    toggleDrawer,
    onCreateClass,
    onFieldChange,
    onDateSelected,
    onStartDateSelected,
    onEndDateSelected,
    onIsClassChange,
    onCellClick,
    onItemSelection,
  } = useClasses();

  return user.isAdmin && (
     <Stack>
      <Stack direction='row' justifyContent='space-between'>
        <AppTextField
          label={t('search')}
          onChange={onSearch}
          InputProps={{
            endAdornment: (
              <InputAdornment position='end'>
                <IconButton edge='end'>
                  <SearchRounded />
                </IconButton>
              </InputAdornment>
            ),
          }}
          sx={{ width: 300 }}
        />
        <Stack direction='row'>
          {rowSelectedValue.length > 0 && (
            <AppButton
              startIcon={<DeleteOutlineRounded />}
              // onClick={onDeleteDepartment}
              color='error'
            >
              {t('delete')}
            </AppButton>
          )}
          <AppButton
            startIcon={<AddRounded />}
            onClick={() => toggleDrawer(true)}
            sx={{ ml: AppMaterial.spacing.verySmall }}
          >
            {t('add')}
          </AppButton>
        </Stack>
      </Stack>
      <ClassTableWidget
        onCellClick={onCellClick}
        onItemSelection={onItemSelection}
        searchText={searchTextValue}
      />
      <AppDrawer
        title={'Create new Class'}
        button={[
          {
            label: t('save'),
            type: 'submit',
            form: 'create-class',
            loading: isSubmit,
          },
        ]}
        open={openDrawer}
        onClose={() => toggleDrawer(false)}
        formId='create-class'
        onSubmit={onCreateClass}
      >
        {createClassFormField.map((el, index) => {
          if (el.type == 'text' || el.type == 'number') {
            return (
              <AppTextField
                key={index}
                defaultValue={classFieldValue[el.name as keyof ClassesModel] ?? null}
                name={el.name}
                label={t(el.title)}
                required={el.isRequired}
                sx={{ mb: AppMaterial.spacing.verySmall }}
                type={el.type}
                onChange={onFieldChange}
              />
            );
          }
          if (el.type == 'date') {
            return (
              <AppDatePicker
                key={index}
                value={
                  classFieldValue[el.name as keyof ClassesModel]
                    ? dayjs(classFieldValue[el.name as keyof ClassesModel])
                    : null
                }
                label={t(el.title)}
                slotProps={{ textField: { required: el.isRequired } }}
                // maxDate={dayjs()}
                onChange={onDateSelected}
              />
            );
          }
          if (el.type == 'select') {
            return (
              <AppTextField
                key={index}
                label={t(el.title)}
                select
                required
                SelectProps={{
                  multiple: false,
                  value: classFieldValue.isClass,
                  onChange: onIsClassChange,
                }}
                sx={{
                  mb: AppMaterial.spacing.verySmall,
                  '& svg': { color: 'rgba(255, 255, 255, 0.4)' },
                }}
              >
                {['yes', 'no'].map((isClass, isClassIndex) => (
                  <MenuItem
                    key={isClassIndex}
                    value={isClass}
                    sx={{
                      '&.Mui-selected': {
                        backgroundColor: 'primary.main',
                        '&:hover': {
                          backgroundColor: 'primary.main',
                        },
                      },
                    }}
                  >
                    {t(isClass)}
                  </MenuItem>
                ))}
              </AppTextField>
            );
          }
        })}
      </AppDrawer>
    </Stack>
  );
}
