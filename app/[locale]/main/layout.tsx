'use client';

import AppButton from '@/components/appButton';
import AppDialog from '@/components/appDialog';
import AppMaterial from '@/material/appMaterial';
import MenuListWidget from '@/views/main/layout/menuListWidget';
import UserInfomationWidget from '@/views/main/layout/userInfomationWidget';
import { LogoutOutlined } from '@mui/icons-material';
import { Grid, Stack, Typography } from '@mui/material';
import { useTranslations } from 'next-intl';
import { redirect, useRouter } from 'next/navigation';
import { useEffect } from 'react';
import { useSignalAndValue } from 'signals-react-safe';
import useMainLayout from './_useMainLayout';

export default function MainLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const t = useTranslations();
  const { _currentUser } = useMainLayout();
  const router = useRouter();
  const [openDialog, openDialogValue] = useSignalAndValue(false);

  useEffect(() => {
    async function init() {
      const jwt = sessionStorage.getItem('jwt');
      if (!jwt) {
        router.replace('/login');
      }
    }
    init();
  }, [router]);

  const onLogOut = async () => {
    sessionStorage.clear();
    router.replace('/login');
    setTimeout(() => {
      window.location.reload();
    }, 200);
  };

  return (
    <Grid container sx={{ height: '100vh' }}>
      <Grid
        item
        md={2}
        sx={{ backgroundColor: AppMaterial.colors.background1 }}
      >
        <Stack justifyContent='space-between' height={'100%'}>
          <Stack>
            <UserInfomationWidget currentUser={_currentUser} />
            <MenuListWidget currentUser={_currentUser} />
          </Stack>
          <Stack>
            <AppButton
              sx={{
                background: 'transparent',
                border: 'none',
                mb: AppMaterial.spacing.verySmall / 2,
                mx: AppMaterial.spacing.verySmall,
              }}
              color='error'
              startIcon={<LogoutOutlined fontSize='small' />}
              onClick={() => (openDialog.value = true)}
            >
              {t('logOut')}
            </AppButton>
            <AppDialog
              open={openDialogValue}
              title={t('logOut')}
              content={<Typography>{t('logOutSub')}</Typography>}
              actionTitle={t('logOut')}
              onSubmit={onLogOut}
              handleClose={() => (openDialog.value = false)}
              actionColor='error'
              cancelColor='secondary'
            />
          </Stack>
        </Stack>
      </Grid>
      <Grid item md={10}>
        <Stack sx={{ p: AppMaterial.spacing.verySmall }}>{children}</Stack>
      </Grid>
    </Grid>
  );
}
