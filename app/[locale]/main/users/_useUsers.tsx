'use client';

import ApiDictionaries from '@/_api/api-dictionaries';
import axiosClient from '@/_api/axios-client';
import { classesList, currentUser, rfidCardList, userList } from '@/store/appStore';
import AppUtils from '@/utils/appUtils';
import { GridCellParams, GridTreeNode } from '@mui/x-data-grid';
import dayjs from 'dayjs';
import { validate } from 'email-validator';
import { useAtom } from 'jotai';
import _ from 'lodash';
import { useTranslations } from 'next-intl';
import { useRouter } from 'next/navigation';
import { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { useSignal, useSignalAndValue } from 'signals-react-safe';

const createUserFormField: AppFieldType[] = [
  {
    title: 'studentCodeAuto',
    name: 'studentCode',
    type: 'text',
    canEdit: false,
    isRequired: false,
  },
  {
    title: 'fullName',
    name: 'fullName',
    type: 'text',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'email',
    name: 'email',
    type: 'email',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'address',
    name: 'address',
    type: 'text',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'phoneNumber',
    name: 'phoneNumber',
    type: 'tel',
    canEdit: true,
    isRequired: true,
  },
  // {
  //   title: 'class',
  //   name: 'class',
  //   type: 'select',
  //   canEdit: false,
  //   isRequired: true,
  // },
  {
    title: 'dob',
    name: 'dob',
    type: 'date',
    canEdit: true,
    isRequired: true,
  },
  {
    title: 'name',
    name: 'roles',
    type: 'select',
    canEdit: false,
    isRequired: true,
  },
  {
    title: 'cardId',
    name: 'cardId',
    type: 'text',
    canEdit: false,
    isRequired: false,
  },
];

export default function useUsers() {
  const t = useTranslations();
  const router = useRouter();
  const [user] = useAtom(currentUser);
  const [users, setUsers] = useAtom(userList);
  const [classes] = useAtom(classesList);
  const [cardIds, setCardIds] = useAtom(rfidCardList);
  const [itemSelected, itemSelectedValue] = useSignalAndValue<UserModel | null>(
    null
  );
  const [rowSelected, rowSelectedValue] = useSignalAndValue<number[]>([]);
  const openDrawer = useSignal<boolean>(false);
  const isSubmit = useSignal<boolean>(false);
  const [userField, userFieldValue] = useSignalAndValue<any>({
    phoneNumber: '',
    email: '',
    fullName: '',
    dob: '',
    address: '',
  });
  const [userRoles, userRolesValue] = useSignalAndValue<string[]>([]);
  const [cardId, cardIdValue] = useSignalAndValue<string>('');
  const [classField, classValue] = useSignalAndValue<string>('');
  const [waitingCardId, waitingCardIdValue] = useSignalAndValue<boolean>(false);
  const [searchText, searchTextValue] = useSignalAndValue<string>('');
  const emailValidate = useSignal<boolean>(false);
  const [ws, wsValue] = useSignalAndValue<WebSocket | null>(null);

  useEffect(() => {
    if (user.studentCode && !user.isAdmin) {
      router.replace('/main/schedules');
      return;
    }
    // console.log(classes);
    
    // WS config
    const socket = new WebSocket("ws://172.20.10.10:8088");
    socket.onopen = (event) => {
      // console.log('Kết nối đã mở.', event);
    };
    socket.onclose = (event) => {
      console.log('Kết nối đã đóng:', event);
    };
    if (socket.readyState === WebSocket.OPEN) {
      console.log('Kết nối đã mở.');
      
    } else {
      console.log('Kết nối không ở trạng thái mở.');
    }
    socket.onmessage = (event) => {
      if (event.data.includes('card_data')) {
        cardId.value = event.data.replace('card_data: ', '').trim();
        waitingCardId.value = false;
      }
    };
    ws.value = socket;
    return () => {
      socket.close();
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router, user,classes]);

  const onSearch = (e: ChangeEvent<HTMLInputElement>) => {
    searchText.value = e.target.value;
  };


  const onCellClick = (
    item: GridCellParams<UserModel, unknown, unknown, GridTreeNode>
  ) => {
    if (item.field !== '__check__') {
      userField.value = {
        phoneNumber: item.row.phoneNumber,
        email: item.row.email,
        fullName: item.row.fullName,
        dob: item.row.dob,
        class: item.row.class,
        address: item.row.address,
        studentCode: item.row.studentCode,
        cardId: item.row.cardId,
      };
      const currentCardId = cardIds.find((el:any) => el.id === item.row.cardId);
      cardId.value = currentCardId?.cardId ?? '';
      itemSelected.value = item.row;
      openDrawer.value = true;
    }
  };

  const toggleDrawer = (value: boolean) => {
    if (!value && itemSelected.value != null) {
      userField.value = {
        phoneNumber: '',
        email: '',
        fullName: '',
        dob: '',
        address: '',
      };
      userRoles.value = [];
      // classField.value = '',
      itemSelected.value = null;
      cardId.value = '';
    }
    openDrawer.value = value;
  };

  const onFieldChange = (e: ChangeEvent<HTMLInputElement>) => {
    const fieldName = e.target.name;

    if (
      fieldName == 'phoneNumber' ||
      fieldName == 'email' ||
      fieldName == 'fullName' ||
      fieldName == 'address'
      //  ||
      // fieldName == 'class'
    ) {
      userField.value[fieldName as keyof UserModel] = e.target.value;
      if (fieldName == 'email' && emailValidate.value) {
        emailValidate.value = false;
      }
    }
  };

  const onDateSelected = (e: any) => {
    userField.value.dob = e;
  };

  const onRolesSelected = (e: any) => {
    const fieldValue = e.target.value;
    userRoles.value = fieldValue;
  };
  /* const onClassSelected = (e: any) => {
    const fieldValue = e.target.value;
    classField.value = fieldValue;
  }; */

  const onCreateUser = async (e: FormEvent) => {
    e.preventDefault();

    if (!validate(userFieldValue.email)) {
      emailValidate.value = true;
      return;
    }
    isSubmit.value = true;

    try {
      if (itemSelectedValue) {
        await updateUser();
      } else {
        await createNewUser();
      }
    } catch (error) {
      console.error(error);
    } finally {
      isSubmit.value = false;
      openDrawer.value = false;
    }
  };


  const createNewUser = async () => {
    const dob = dayjs(userFieldValue.dob).format('YYYY-MM-DD');
    let newCardId = null;
    if (cardIdValue) {
      const cardIdData = await axiosClient.post(ApiDictionaries.RFID_CARD(), {
        data: { cardId: cardIdValue },
      });

      if (cardIdData) {
        newCardId = cardIdData.data.id;
      }
    }

    const data: any = await axiosClient.post(ApiDictionaries.USER(), {
      ...userFieldValue,
      identifier: userFieldValue.email,
      username: userFieldValue.email,
      role: 1,
      class: '',
      password: '123456',
      isAdmin: userRolesValue.includes('admin'),
      isTeacher: userRolesValue.includes('teacher'),
      isStudent: userRolesValue.includes('student'),
      dob: dob,
      cardId: newCardId,
    });

    if (data) {
      const cardIdData = await axiosClient.put(
        ApiDictionaries.RFID_CARD(newCardId.toString()),
        {
          data: { user: data.id },
        }
      );
      if (cardIdData) {
        const cardIdsClone = _.cloneDeep(cardIds);
        if (cardIdsClone.findIndex((el:any) => el.id === cardIdData.data.id) < 0) {
          cardIdsClone.unshift({
            id: cardIdData.data.id,
            cardId: cardIdData.data.attributes.cardId,
            user: cardIdData.data.attributes.user.data?.id,
          });
          setCardIds(cardIdsClone);
        }
      }

      const studentCode = String(data.id).padStart(7, '0');
      const resUpdate = await axiosClient.put(ApiDictionaries.USER(data.id), {
        studentCode,
        username: studentCode,
      });

      if (resUpdate) {
        const newUser = {
          id: data.id,
          email: data.email,
          phoneNumber: data.phoneNumber,
          fullName: data.fullName,
          studentCode: studentCode,
          checkIn: data.checkIn ?? [],
          checkOut: data.checkOut ?? [],
          dob: data.dob ? AppUtils.dateFormat(data.dob) : '',
          address: data.address,
          class: data.class,
          cardId: data.cardId,
          schedules: user.schedules,
          isAdmin: data.isAdmin ?? false,
          isStudent: data.isStudent ?? false,
          isTeacher: data.isTeacher ?? false,
          createdAt: data.createdAt,
        };
        const usersClone = _.cloneDeep(users);
        usersClone.unshift(newUser);
        setUsers(usersClone);
      }
    }
  };

  const updateUser = async () => {
    const dob = dayjs(userFieldValue.dob).format('YYYY-MM-DD');
    let newCardId = null;
    if (cardIdValue) {
      const cardIdData = await axiosClient.post(ApiDictionaries.RFID_CARD(), {
        data: { cardId: cardIdValue },
      });

      if (cardIdData) {
        newCardId = cardIdData.data.id;
      }
    }

    const data: any = await axiosClient.put(
      ApiDictionaries.USER(itemSelectedValue?.id?.toString()),
      {
        ...userFieldValue,
        identifier: userFieldValue.email,
        isAdmin: userRolesValue.includes('admin'),
        isTeacher: userRolesValue.includes('teacher'),
        isStudent: userRolesValue.includes('student'),
        dob: dob,
        cardId: newCardId,
      }
    );

    if (data) {
      const cardIdData = await axiosClient.put(
        ApiDictionaries.RFID_CARD(newCardId.toString()),
        {
          data: { user: data.id },
        }
      );
      if (cardIdData) {
        const cardIdsClone = _.cloneDeep(cardIds);
        if (cardIdsClone.findIndex((el:any) => el.id === cardIdData.data.id) < 0) {
          cardIdsClone.unshift({
            id: cardIdData.data.id,
            cardId: cardIdData.data.attributes.cardId,
            user: cardIdData.data.attributes.user.data?.id,
          });
          setCardIds(cardIdsClone);
        }
      }
      
      const usersClone = _.cloneDeep(users);
      const userIndex = usersClone.findIndex(
        (el:any) => el.id == itemSelectedValue?.id
      );
      if (userIndex > -1) {
        const newUser = {
          id: data.id,
          email: data.email,
          phoneNumber: data.phoneNumber,
          fullName: data.fullName,
          studentCode: data.studentCode,
          dob: data.dob ? AppUtils.dateFormat(data.dob) : '',
          address: data.address,
          checkIn: data.checkIn ?? [],
          checkOut: data.checkOut ?? [],
          cardId: data.cardId,
          class: data.class,
          schedules: user.schedules,
          isAdmin: data.isAdmin ?? false,
          isStudent: data.isStudent ?? false,
          isTeacher: data.isTeacher ?? false,
          createdAt: data.createdAt,
        };

        usersClone[userIndex] = newUser;
        setUsers(usersClone);
      }
    }
  };

  const onRequestCardID = () => {
    if (wsValue) {
      waitingCardId.value = true;
      wsValue.send('get_card_id');
    }
  };

  const onItemSelection = (e: any) => {
    rowSelected.value = e;
  };

  const onDeleteUser = () => {
    const usersClone = _.cloneDeep(users);

    rowSelectedValue.forEach(async (item) => {
      const userIndex = usersClone.findIndex((el:any) => el.id == item);
      if (userIndex > -1) {
        usersClone.splice(userIndex, 1);
        setUsers(usersClone);
        await axiosClient.delete(ApiDictionaries.USER(item.toString()));
      }
    });

    rowSelected.value = [];
  };

  return {
    t,
    openDrawer,
    createUserFormField,
    userFieldValue,
    userRolesValue,
    isSubmit,
    classes,
    emailValidate,
    rowSelectedValue,
    itemSelectedValue,
    searchTextValue,
    user,
    
    wsValue,
    cardIdValue,
    waitingCardIdValue,
    // onClassSelected,
    toggleDrawer,
    onSearch,
    onCellClick,
    onCreateUser,
    onFieldChange,
    onRolesSelected,
    onDateSelected,
    onRequestCardID,
    onDeleteUser,
    onItemSelection,
  };
}
