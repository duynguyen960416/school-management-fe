'use client';

import AppButton from '@/components/appButton';
import AppDatePicker from '@/components/appDatePicker';
import AppDrawer from '@/components/appDrawer';
import AppTextField from '@/components/appTextField';
import AppMaterial from '@/material/appMaterial';
import UserTableWidget from '@/views/main/users/userTableWidget';
import {
  AddRounded,
  DeleteOutlineRounded,
  SearchRounded,
} from '@mui/icons-material';
import {
  Grid,
  IconButton,
  InputAdornment,
  MenuItem,
  Stack,
} from '@mui/material';
import dayjs from 'dayjs';
import useUsers from './_useUsers';

export default function UserManagement() {
  const {
    t,
    openDrawer,
    createUserFormField,
    userFieldValue,
    userRolesValue,
    classes,
    isSubmit,
    rowSelectedValue,
    emailValidate,
    itemSelectedValue,
    searchTextValue,
    user,
    wsValue,
    cardIdValue,
    waitingCardIdValue,
    toggleDrawer,
    onCellClick,
    onSearch,
    onCreateUser,
    onFieldChange,
    // onClassSelected,
    onRolesSelected,
    onDateSelected,
    onRequestCardID,
    onDeleteUser,
    onItemSelection,
  } = useUsers();

  return (
    user.isAdmin && (
      <Stack>
        <Stack direction='row' justifyContent='space-between'>
          <AppTextField
            label={t('search')}
            onChange={onSearch}
            InputProps={{
              endAdornment: (
                <InputAdornment position='end'>
                  <IconButton edge='end'>
                    <SearchRounded />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            sx={{ width: 300 }}
          />
          <Stack direction='row'>
            {rowSelectedValue.length > 0 && (
              <AppButton
                startIcon={<DeleteOutlineRounded />}
                onClick={onDeleteUser}
                color='error'
              >
                {t('delete')}
              </AppButton>
            )}
            <AppButton
              startIcon={<AddRounded />}
              onClick={() => toggleDrawer(true)}
              sx={{ ml: AppMaterial.spacing.verySmall }}
            >
              {t('add')}
            </AppButton>
          </Stack>
        </Stack>
        <UserTableWidget
          onCellClick={onCellClick}
          onItemSelection={onItemSelection}
          searchText={searchTextValue}
        />
        <AppDrawer
          title={t('createNewUser')}
          button={[
            {
              label: t('save'),
              type: 'submit',
              form: 'create-user',
              loading: isSubmit,
            },
          ]}
          open={openDrawer}
          onClose={() => toggleDrawer(false)}
          formId='create-user'
          onSubmit={onCreateUser}
        >
          {createUserFormField.map((el, index) => {
            if (
              (el.type == 'text' || el.type == 'email' || el.type == 'tel') &&
              el.name != 'cardId'
            ) {
              return (
                <AppTextField
                  key={index}
                  defaultValue={
                    userFieldValue[el.name as keyof UserModel] ?? null
                  }
                  name={el.name}
                  label={t(el.title)}
                  required={el.isRequired}
                  sx={{ mb: AppMaterial.spacing.verySmall }}
                  type={el.type}
                  onChange={onFieldChange}
                  signalemailerror={emailValidate}
                  disabled={el.name == 'studentCode'}
                />
              );
            }
          
            if (el.type == 'date') {
              return (
                <AppDatePicker
                  key={index}
                  value={
                    userFieldValue[el.name as keyof UserModel]
                      ? dayjs(userFieldValue[el.name as keyof UserModel])
                      : null
                  }
                  label={t(el.title)}
                  slotProps={{ textField: { required: el.isRequired } }}
                  maxDate={dayjs()}
                  onChange={onDateSelected}
                />
              );
            }
            if ((el.type == 'select') && el.name == 'roles') {
            
                let defaultRoles = userRolesValue;
              if (itemSelectedValue?.isAdmin) {
                defaultRoles.push('admin');
              }
              if (itemSelectedValue?.isStudent) {
                defaultRoles.push('student');
              }
              if (itemSelectedValue?.isTeacher) {
                defaultRoles.push('teacher');
              }

              return (
                <AppTextField
                  key={index}
                  label={t(el.title)}
                  select
                  required
                  SelectProps={{
                    multiple: true,
                    value: defaultRoles,
                    onChange: onRolesSelected,
                  }}
                  sx={{
                    mb: AppMaterial.spacing.verySmall,
                    '& svg': { color: 'rgba(255, 255, 255, 0.4)' },
                  }}
                >
                  {['admin', 'teacher', 'student'].map((role, roleIndex) => (
                    <MenuItem
                      key={roleIndex}
                      value={role}
                      sx={{
                        '&.Mui-selected': {
                          backgroundColor: 'primary.main',
                          '&:hover': {
                            backgroundColor: 'primary.main',
                          },
                        },
                      }}
                    >
                      {t(role)}
                    </MenuItem>
                  ))}
                </AppTextField>
              );
            }
            /* if (el.name == 'class') {
              let classOptions = classes;
              return (
                <AppTextField
                  key={index}
                  label={t(el.title)}
                  select
                  required
                  SelectProps={{
                    multiple: false,
                    value: classes,
                    onChange: onClassSelected,
                  }}
                  sx={{
                    mb: AppMaterial.spacing.verySmall,
                    '& svg': { color: 'rgba(255, 255, 255, 0.4)' },
                  }}
                >
                  {classOptions.map((classItem) => (
                    <MenuItem
                      key={classItem.id}
                      value={classItem.name}
                      sx={{
                        '&.Mui-selected': {
                          backgroundColor: 'primary.main',
                          '&:hover': {
                            backgroundColor: 'primary.main',
                          },
                        },
                      }}
                    >
                      {t(classItem.name)}
                    </MenuItem>
                  ))}
                </AppTextField>
              );
            
            } */
            if (el.name == 'cardId') {
              return (
                <Grid key={index} container spacing={2}>
                  <Grid item xs={9}>
                    <AppTextField
                      key={index}
                      name={el.name}
                      value={cardIdValue}
                      label={t(el.title)}
                      sx={{ mb: AppMaterial.spacing.verySmall }}
                      type={el.type}
                      onChange={onFieldChange}
                      disabled
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <AppButton
                      onClick={onRequestCardID}
                      sx={{ height: 37.17 }}
                      disabled={!wsValue}
                      loading={waitingCardIdValue}
                    >
                      {t('addCard')}
                    </AppButton>
                  </Grid>
                </Grid>
              );
            }
          }
        )}
        </AppDrawer>
      </Stack>
    ))}