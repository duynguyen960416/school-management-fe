import { JotaiProvider } from '@/store/appStore';
import ThemeRegistry from '@/theme/themeRegistry';
import { NextIntlClientProvider } from 'next-intl';
import { Noto_Sans } from 'next/font/google';
import { notFound } from 'next/navigation';
import '../../globals.css';

const roboto = Noto_Sans({
  weight: ['300', '600', '900'],
  subsets: ['vietnamese', 'latin'],
});

export const metadata = {
  title: 'SCHOOL MANAGEMENT',
  description: '',
};

const locales = ['vi', 'en'];
const timeZone = 'Asia/Bangkok';

export default async function RootLayout({
  children,
  params: { locale },
}: {
  children: React.ReactNode;
  params: { locale: string };
}) {
  let messages;
  try {
    messages = (await import(`../../messages/${locale}.json`)).default;
  } catch (error) {
    console.log(error);
  }
  if (!locales.includes(locale as any)) notFound();

  return (
    <html lang={locale}>
      <ThemeRegistry>
        <body suppressHydrationWarning={true}>
          <NextIntlClientProvider
            locale={locale}
            messages={messages}
            timeZone={timeZone}
          >
            <JotaiProvider>
              <main className={roboto.className}>{children}</main>
            </JotaiProvider>
          </NextIntlClientProvider>
        </body>
      </ThemeRegistry>
    </html>
  );
}
