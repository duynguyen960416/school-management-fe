import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';
import { useTranslations } from 'next-intl';
import AppButton from './appButton';

export default function AppDialog({
  open,
  title,
  content,
  actionTitle,
  actionColor,
  cancelColor,
  actionDisabled,
  handleClose,
  onSubmit,
}: {
  open: boolean;
  title: string;
  content: React.ReactNode;
  actionTitle: string;
  actionColor?:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'error'
    | 'info'
    | 'warning';
  cancelColor?:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'error'
    | 'info'
    | 'warning';
  actionDisabled?: boolean;
  handleClose: () => void;
  onSubmit: () => Promise<void>;
}) {
  const t = useTranslations();

  const onAction = async () => {
    await onSubmit();
    handleClose();
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>{content}</DialogContent>
      <DialogActions>
        <AppButton
          onClick={handleClose}
          sx={{ textTransform: 'none' }}
          color={cancelColor ?? 'error'}
        >
          {t('cancel')}
        </AppButton>
        <AppButton
          onClick={onAction}
          autoFocus
          sx={{ textTransform: 'none' }}
          color={actionColor ?? 'secondary'}
          disabled={actionDisabled ?? false}
        >
          {actionTitle}
        </AppButton>
      </DialogActions>
    </Dialog>
  );
}
