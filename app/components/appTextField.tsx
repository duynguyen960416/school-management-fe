import AppMaterial from '@/material/appMaterial';
import { TextField, TextFieldProps } from '@mui/material';
import { Signal, signal, useSignalValue } from 'signals-react-safe';

type AppTextFieldType = {
  signalemailerror?: Signal<boolean>;
} & TextFieldProps;

export default function AppTextField(props: AppTextFieldType) {
  const error = useSignalValue(
    props.signalemailerror ?? signal<boolean>(false)
  );

  let newProps = { ...props };
  newProps.sx = {
    ...AppMaterial.inputStyle,
    ...props.sx,
  };

  return (
    <TextField
      size='small'
      fullWidth
      variant='outlined'
      autoComplete='off'
      error={props.type == 'email' && error}
      {...newProps}
    />
  );
}
