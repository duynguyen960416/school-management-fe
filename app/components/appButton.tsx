import AppMaterial from '@/material/appMaterial';
import { LoadingButton, LoadingButtonProps } from '@mui/lab';
import { CircularProgress } from '@mui/material';
import { Signal, signal, useSignal, useSignalValue } from 'signals-react-safe';

interface AppButtonType extends LoadingButtonProps {
  signalloading?: Signal<boolean>;
  signaldisabled?: Signal<boolean>;
}

export default function AppButton(props: AppButtonType) {
  const loading = useSignalValue(props.signalloading ?? signal(false));
  const disabled = useSignalValue(props.signaldisabled ?? signal(false));

  let newProps = { ...props };
  newProps.sx = {
    borderRadius: 2,
    border: '1px solid rgba(255, 255, 255, 0.2)',
    px: 3,
    py: 1,
    fontSize: AppMaterial.fontSize.small,
    textTransform: 'none',
    '&.MuiButton-root.Mui-disabled': {
      color: 'rgba(255, 255, 255, 0.4) !important',
    },
    ...props.sx,
  };

  return (
    <LoadingButton
      variant='contained'
      loadingIndicator={<CircularProgress color='info' size={20} />}
      loading={props.loading ?? loading}
      disabled={props.disabled ?? disabled}
      color={props.color ?? 'secondary'}
      {...newProps}
    />
  );
}
