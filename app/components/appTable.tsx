import AppMetadata from '@/appMetadata/appMetadata';
import { LinearProgress } from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { Localization } from '@mui/x-data-grid/utils/getGridLocalization';
import { Signal, useSignalValue } from 'signals-react-safe';

export default function AppTable({
  rows,
  columns,
  gridLocalization,
  onCellClick,
  onItemSelection,
}: {
  rows: any[],
  columns: GridColDef[];
  gridLocalization: Signal<Localization>;
  onCellClick?: (e: any) => void;
  onItemSelection?: (e: any) => void;
}) {
  const _gridLocalization = useSignalValue(gridLocalization);

  return (
    <DataGrid
      rows={rows}
      columns={columns}
      autoHeight={true}
      getRowHeight={() => 'auto'}
      initialState={AppMetadata.tableInitialState}
      pageSizeOptions={AppMetadata.tableSizeOptions}
      disableColumnFilter
      disableColumnMenu
      checkboxSelection
      disableRowSelectionOnClick
      isCellEditable={() => false}
      onCellClick={onCellClick}
      localeText={
        _gridLocalization.components.MuiDataGrid.defaultProps.localeText
      }
      slots={{ loadingOverlay: LinearProgress }}
      onRowSelectionModelChange={onItemSelection}
    />
  );
}
