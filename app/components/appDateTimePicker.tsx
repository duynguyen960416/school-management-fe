import AppMaterial from '@/material/appMaterial';
import {
  DateTimePicker,
  DateTimePickerProps,
  LocalizationProvider,
} from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';

export default function AppTimePicker(props: DateTimePickerProps<any>) {
  let newProps = { ...props };
  newProps.sx = {
    height: 40,
    mb: AppMaterial.spacing.verySmall,

    ...AppMaterial.inputStyle,
    ...props.sx,
  };

  newProps.slotProps = {
    textField: { size: 'small', ...newProps.slotProps?.textField },
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DateTimePicker {...newProps} />
    </LocalizationProvider>
  );
}
