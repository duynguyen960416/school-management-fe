import AppMaterial from '@/material/appMaterial';
import {
  LocalizationProvider,
  TimePicker,
  TimePickerProps,
  renderTimeViewClock,
} from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';

export default function AppTimePicker(props: TimePickerProps<any>) {
  let newProps = { ...props };
  newProps.sx = {
    height: 40,
    mb: AppMaterial.spacing.verySmall,

    ...AppMaterial.inputStyle,
    ...props.sx,
  };

  newProps.slotProps = {
    textField: { size: 'small', ...newProps.slotProps?.textField },
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <TimePicker {...newProps} />
    </LocalizationProvider>
  );
}
