import AppMaterial from '@/material/appMaterial';
import { Divider, Drawer, Stack, Typography } from '@mui/material';
import { useTranslations } from 'next-intl';
import { ReactNode } from 'react';
import { Signal, useSignalValue } from 'signals-react-safe';
import AppButton from './appButton';

interface AppDrawerButtonType {
  label: string;
  color?:
    | 'inherit'
    | 'error'
    | 'primary'
    | 'secondary'
    | 'info'
    | 'success'
    | 'warning';
  type?: 'submit' | 'button';
  form?: string;
  action?: (e: any) => void;
  loading?: Signal<boolean>;
}

export default function AppDrawer({
  open,
  onClose,
  children,
  title,
  button,
  formId,
  onSubmit,
}: {
  open: Signal<boolean>;
  onClose: () => void;
  onSubmit: (e: any) => void;
  children: ReactNode;
  title: string;
  button: AppDrawerButtonType[];
  formId?: string;
}) {
  const t = useTranslations();
  const openItemModal = useSignalValue(open);

  return (
    <Drawer anchor='right' open={openItemModal} onClose={onClose}>
      <Stack
        sx={{
          minWidth: 400,
          height: '100%',
          backgroundColor: AppMaterial.colors.background1,
        }}
      >
        <Typography sx={{ p: AppMaterial.spacing.verySmall }}>
          {title}
        </Typography>
        <Divider />
        <Stack
          sx={{
            p: AppMaterial.spacing.verySmall,
            overflowY: 'auto',
            flexGrow: 1,
          }}
          id={formId}
          component='form'
          onSubmit={onSubmit}
        >
          {children}
        </Stack>
        <Divider />
        <Stack
          direction='row'
          sx={{ p: AppMaterial.spacing.verySmall, justifyContent: 'flex-end' }}
        >
          <AppButton color='primary' onClick={onClose}>{t('cancel')}</AppButton>
          {button.map((el, index) => (
            <AppButton
              key={index}
              color={el.color ?? 'secondary'}
              onClick={el.action}
              signalloading={el.loading}
              sx={{ ml: AppMaterial.spacing.verySmall }}
              type={el.type ?? 'button'}
              form={el.form}
            >
              {el.label}
            </AppButton>
          ))}
        </Stack>
      </Stack>
    </Drawer>
  );
}
