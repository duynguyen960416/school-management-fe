interface UserModel {
  id?: number;
  phoneNumber: string;
  email: string;
  fullName: string;
  studentCode: string;
  dob: string;
  class: number;
  checkIn: CheckInModel[];
  checkOut: CheckInModel[];
  address: string;
  cardId?: number;
  schedules: number[];
  isAdmin: boolean;
  isStudent: boolean;
  isTeacher: boolean;
  createdAt?: string;
}

interface AppFieldType {
  title: string;
  name: string;
  type: 'text' | 'tel' | 'date' | 'checkbox' | 'email' | 'select' | 'number';
  canEdit: boolean;
  isRequired: boolean;
}

interface DepartmentModel {
  id?: number;
  name: string;
  isClass: boolean;
  capacity: number;
}

interface ClassesModel {
  id?: number;
  name: string;
  startDate: string;
  endDate: string;
  users:number,
  schedules: number
}

interface RfidCardModel {
  id?: number;
  cardId: string;
  user: number;
}

interface ScheduleModel {
  id?: number;
  date: string;
  time: string;
  duration: number;
  class: number;
  week: number;
  subject: number;
  users: number[];
  checkIn: CheckInModel[];
  checkOut: CheckInModel[];
  department: number;
}

interface SubjectModel {
  id?: number;
  name: string;
  credit: number;
  schedules: number[];
}

interface WeekModel {
  id?: number;
  name: string;
  startDate: string;
  endDate: string;
  schedules: number[];
}

interface CheckInModel {
  id?: number;
  user: number;
  department: number;
  dateTime: string;
}

interface CheckOutModel {
  id?: number;
  user: number;
  department: number;
  dateTime: string;
}

