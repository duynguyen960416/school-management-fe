'use client';

import { Provider, atom } from 'jotai';
import { ReactNode } from 'react';

export const JotaiProvider = ({ children }: { children: ReactNode }) => {
  return <Provider>{children}</Provider>;
};

export const currentUser = atom<UserModel>({
  phoneNumber: '',
  email: '',
  fullName: '',
  checkIn: [],
  checkOut: [],
  studentCode: '',
  dob: '',
  class: '',
  address: '',
  cardId: 0,
  schedules: [],
  isAdmin: false,
  isStudent: false,
  isTeacher: false
});

export const userList = atom<UserModel[]>([]);
export const departmentList = atom<DepartmentModel[]>([]);
export const classesList = atom<ClassesModel[]>([]);
export const scheduleList = atom<ScheduleModel[]>([]);
export const subjectList = atom<SubjectModel[]>([]);
export const rfidCardList = atom<RfidCardModel[]>([]);
export const weekList = atom<WeekModel[]>([]);
export const checkInList = atom<CheckInModel[]>([]);
export const checkOutList = atom<CheckInModel[]>([]);
