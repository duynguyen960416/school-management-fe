import { ThemeOptions } from "@mui/material/styles";

export const themeOptions: ThemeOptions = {
  palette: {
    background: {
      default: "#151C2D",
    },
    primary: {
      main: "#151C2D",
      contrastText: "#FFFFFF",
    },
    secondary: {
      main: '#047A79',
    },
    error: {
      main: '#FF3333',
    },
    info: {
      main: '#5BC0DE'
    },
    text: {
      primary: "#FFFFFF",
    },
  },
  spacing: (abs: number) => `${4 * abs}px`,
  components: {
    MuiButton: {
      styleOverrides: {
        text: {
          minWidth: 0,
          padding: 0,
          '&:hover': {
            backgroundColor: 'transparent',
          }
        },
        contained: {
          boxShadow: 'none',
          '@media (hover: hover)': {
            '&:hover': {
              boxShadow: 'none',
            },
          },
        }
      }
    },
    MuiToolbar: {
      styleOverrides: {
        root: {
          padding: '0 !important',
        }
      }
    }
  },
  typography: {
    fontSize: 14,
    body1: {
      fontSize: 14,
      color: "#FFFFFF",
    },
    h5: {
      fontSize: 18,
      color: "#FFFFFF",
      fontWeight: 600,
    }
  },
};