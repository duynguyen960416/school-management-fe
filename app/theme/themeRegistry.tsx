'use client';

import { ThemeProvider, createTheme } from '@mui/material/styles';
import { ReactNode } from 'react';
import { NextAppDirEmotionCacheProvider } from './emotionCache';
import { themeOptions } from './themeOptions';
import CssBaseline from '@mui/material/CssBaseline';

const theme = createTheme(themeOptions);

export default function ThemeRegistry({ children }: { children: ReactNode }) {
  return (
    <NextAppDirEmotionCacheProvider options={{ key: 'mui' }}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </NextAppDirEmotionCacheProvider>
  );
}
